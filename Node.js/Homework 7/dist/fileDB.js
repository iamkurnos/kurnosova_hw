"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.database = void 0;
const fs_1 = require("fs");
class Database {
    constructor() {
        this.tables = {};
    }
    static getInstance() {
        if (!Database.instance) {
            Database.instance = new Database();
        }
        return Database.instance;
    }
    registerSchema(schemaName, schema) {
        if (!this.tables[schemaName]) {
            this.tables[schemaName] = new Table(schemaName, schema);
        }
    }
    getTable(schemaName) {
        return this.tables[schemaName];
    }
}
class Table {
    constructor(schemaName, schema) {
        this.schemaName = schemaName;
        this.schema = schema;
    }
    async getAll() {
        const rawData = await this.loadData();
        return rawData;
    }
    async getById(id) {
        const rawData = await this.loadData();
        return rawData.find((item) => item.id === id) || null;
    }
    async create(data) {
        const rawData = await this.loadData();
        data.id = Date.now();
        rawData.push(data);
        await this.saveData(rawData);
        return data;
    }
    async update(id, updatedFields) {
        const rawData = await this.loadData();
        const itemIndex = rawData.findIndex((item) => item.id === id);
        if (itemIndex === -1) {
            return null;
        }
        const filteredData = {};
        for (const field in this.schema) {
            if (updatedFields.hasOwnProperty(field)) {
                filteredData[field] = updatedFields[field];
            }
        }
        rawData[itemIndex] = { ...rawData[itemIndex], ...filteredData };
        await this.saveData(rawData);
        return rawData[itemIndex];
    }
    async delete(id) {
        const rawData = await this.loadData();
        const itemIndex = rawData.findIndex((item) => item.id === id);
        if (itemIndex === -1) {
            return null;
        }
        const deletedId = rawData[itemIndex].id;
        rawData.splice(itemIndex, 1);
        await this.saveData(rawData);
        return deletedId;
    }
    async loadData() {
        try {
            const rawData = await fs_1.promises.readFile(`${this.schemaName}.json`, "utf-8");
            return JSON.parse(rawData);
        }
        catch (error) {
            return [];
        }
    }
    async saveData(data) {
        await fs_1.promises.writeFile(`${this.schemaName}.json`, JSON.stringify(data, null, 2));
    }
}
exports.database = Database.getInstance();
