"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fileDB_1 = require("./fileDB");
const newspostSchema = {
    title: String,
    text: String,
};
function initiateSchemes() {
    fileDB_1.database.registerSchema("newspost", newspostSchema);
}
exports.default = initiateSchemes;
