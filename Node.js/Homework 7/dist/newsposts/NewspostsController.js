"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const NewspostsService_1 = __importDefault(require("./NewspostsService"));
const express_1 = __importDefault(require("express"));
const typedi_1 = require("typedi");
let NewspostsController = class NewspostsController {
    constructor(service) {
        this.service = service;
        this.path = "/newsposts";
        this.router = express_1.default.Router();
        this.getAll = async (req, res) => {
            try {
                const params = req.query;
                const news = await this.service.getAll(params);
                res.status(200).json(news);
            }
            catch (error) {
                console.log(error);
                res.status(500).json({ error: "Internal server error" });
            }
        };
        this.getById = async (req, res) => {
            const postId = +req.params.id;
            try {
                const newsPost = await this.service.getById(postId);
                if (!newsPost) {
                    res.status(404).json({ error: "News post not found" });
                    return;
                }
                res.status(200).json(newsPost);
            }
            catch (error) {
                res.status(500).json({ error: "Internal server error" });
            }
        };
        this.create = async (req, res) => {
            const newData = req.body;
            if (!newData.title || !newData.text) {
                res.status(400).json({ error: "Invalid request body" });
                return;
            }
            try {
                const createdNewsPost = await this.service.create(newData);
                res.status(200).json(createdNewsPost);
            }
            catch (error) {
                res.status(500).json({ error: "Internal server error" });
            }
        };
        this.update = async (req, res) => {
            const postId = +req.params.id;
            const updatedFields = req.body;
            try {
                const updatedNewsPost = await this.service.update(postId, updatedFields);
                if (!updatedNewsPost) {
                    res.status(404).json({ error: "News post not found" });
                    return;
                }
                res.status(200).json(updatedNewsPost);
            }
            catch (error) {
                res.status(500).json({ error: "Internal server error" });
            }
        };
        this.delete = async (req, res) => {
            const postId = +req.params.id;
            try {
                const deletedId = await this.service.delete(postId);
                if (!deletedId) {
                    res.status(404).json({ error: "News post not found" });
                    return;
                }
                res.status(200).json({ message: "News post deleted successfully" });
            }
            catch (error) {
                res.status(500).json({ error: "Internal server error" });
            }
        };
        this.intializeRoutes();
    }
    intializeRoutes() {
        this.router.get(this.path, this.getAll);
        this.router.get(this.path + "/:id", this.getById);
        this.router.post(this.path, this.create);
        this.router.put(this.path + "/:id", this.update);
        this.router.delete(this.path + "/:id", this.delete);
    }
};
NewspostsController = __decorate([
    (0, typedi_1.Service)(),
    __metadata("design:paramtypes", [NewspostsService_1.default])
], NewspostsController);
exports.default = NewspostsController;
