"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const NewspostsRepository_1 = __importDefault(require("../repositories/NewspostsRepository"));
const typedi_1 = require("typedi");
let NewspostsService = class NewspostsService {
    constructor(repository) {
        this.repository = repository;
    }
    async getAll(params) {
        try {
            return await this.repository.getAll(params);
        }
        catch (error) {
            throw error;
        }
    }
    async getById(id) {
        try {
            return await this.repository.getById(id);
        }
        catch (error) {
            throw error;
        }
    }
    async create(data) {
        try {
            return await this.repository.create(data);
        }
        catch (error) {
            throw error;
        }
    }
    async update(id, update) {
        try {
            return await this.repository.update(id, update);
        }
        catch (error) {
            throw error;
        }
    }
    async delete(id) {
        try {
            return await this.repository.delete(id);
        }
        catch (error) {
            throw error;
        }
    }
};
NewspostsService = __decorate([
    (0, typedi_1.Service)(),
    __metadata("design:paramtypes", [NewspostsRepository_1.default])
], NewspostsService);
exports.default = NewspostsService;
