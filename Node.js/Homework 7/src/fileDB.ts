import { promises as fs } from "fs";

interface Schema {
   [key: string]: any;
}

class Database {
   private static instance: Database;
   private tables: { [key: string]: Table } = {};

   private constructor() {}

   public static getInstance(): Database {
      if (!Database.instance) {
         Database.instance = new Database();
      }
      return Database.instance;
   }

   public registerSchema(schemaName: string, schema: Schema): void {
      if (!this.tables[schemaName]) {
         this.tables[schemaName] = new Table(schemaName, schema);
      }
   }

   public getTable(schemaName: string): Table {
      return this.tables[schemaName];
   }
}

class Table {
   constructor(private schemaName: string, private schema: Schema) {}

   public async getAll() {
      const rawData = await this.loadData();
      return rawData;
   }

   public async getById(id: number): Promise<any | null> {
      const rawData = await this.loadData();
      return rawData.find((item: Schema) => item.id === id) || null;
   }

   public async create(data: Schema): Promise<any> {
      const rawData = await this.loadData();
      data.id = Date.now();
      rawData.push(data);
      await this.saveData(rawData);
      return data;
   }

   public async update(id: number, updatedFields: Schema): Promise<any | null> {
      const rawData = await this.loadData();
      const itemIndex = rawData.findIndex((item: Schema) => item.id === id);
      if (itemIndex === -1) {
         return null;
      }
      const filteredData: Schema = {};
      for (const field in this.schema) {
         if (updatedFields.hasOwnProperty(field)) {
            filteredData[field] = updatedFields[field];
         }
      }
      rawData[itemIndex] = { ...rawData[itemIndex], ...filteredData };
      await this.saveData(rawData);
      return rawData[itemIndex];
   }

   public async delete(id: number): Promise<number | null> {
      const rawData = await this.loadData();
      const itemIndex = rawData.findIndex((item) => item.id === id);
      if (itemIndex === -1) {
         return null;
      }
      const deletedId = rawData[itemIndex].id;
      rawData.splice(itemIndex, 1);
      await this.saveData(rawData);
      return deletedId;
   }

   private async loadData(): Promise<Schema[]> {
      try {
         const rawData = await fs.readFile(`${this.schemaName}.json`, "utf-8");
         return JSON.parse(rawData);
      } catch (error) {
         return [];
      }
   }

   private async saveData(data: Schema[]): Promise<void> {
      await fs.writeFile(`${this.schemaName}.json`, JSON.stringify(data, null, 2));
   }
}

export const database = Database.getInstance();
export {Schema}
