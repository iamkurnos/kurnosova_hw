import { log } from "console";
import NewspostsService from "./NewspostsService";
import express, { Request, Response } from "express";
import { Service } from "typedi";

@Service()
class NewspostsController {
   public path = "/newsposts";
   public router = express.Router();

   constructor(private service: NewspostsService) {
      this.intializeRoutes();
   }

   public intializeRoutes() {
      this.router.get(this.path, this.getAll);
      this.router.get(this.path + "/:id", this.getById);
      this.router.post(this.path, this.create);
      this.router.put(this.path + "/:id", this.update);
      this.router.delete(this.path + "/:id", this.delete);
   }

   getAll = async (req: Request, res: Response) =>{
      try {
         const params = req.query;
         const news = await this.service.getAll(params);
         res.status(200).json(news);
      } catch (error) {
         console.log(error);
         
         res.status(500).json({ error: "Internal server error" });
      }
   }

   getById = async(req: Request, res: Response) => {
      const postId = +req.params.id;
      try {
         const newsPost = await this.service.getById(postId);
         if (!newsPost) {
            res.status(404).json({ error: "News post not found" });
            return;
         }
         res.status(200).json(newsPost);
      } catch (error) {
         res.status(500).json({ error: "Internal server error" });
      }
   }

   create = async(req: Request, res: Response) => {
      const newData = req.body;

      if (!newData.title || !newData.text) {
         res.status(400).json({ error: "Invalid request body" });
         return;
      }
      try {
         const createdNewsPost = await this.service.create(newData);
         res.status(200).json(createdNewsPost);
      } catch (error) {
         res.status(500).json({ error: "Internal server error" });
      }
   }

   update = async(req: Request, res: Response) =>{
      const postId = +req.params.id;
      const updatedFields = req.body;
      try {
         const updatedNewsPost = await this.service.update(postId, updatedFields);
         if (!updatedNewsPost) {
            res.status(404).json({ error: "News post not found" });
            return;
         }
         res.status(200).json(updatedNewsPost);
      } catch (error) {
         res.status(500).json({ error: "Internal server error" });
      }
   }

   delete = async(req: Request, res: Response)=> {
      const postId = +req.params.id;

      try {
         const deletedId = await this.service.delete(postId);
         if (!deletedId) {
            res.status(404).json({ error: "News post not found" });
            return;
         }
         res.status(200).json({ message: "News post deleted successfully" });
      } catch (error) {
         res.status(500).json({ error: "Internal server error" });
      }
   }
}

export default NewspostsController;
