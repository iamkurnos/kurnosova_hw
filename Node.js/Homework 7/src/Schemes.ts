import { database, Schema } from "./fileDB";

const newspostSchema: Schema = {
   title: String,
   text: String,
};

export default function initiateSchemes (){
    database.registerSchema("newspost", newspostSchema);
}