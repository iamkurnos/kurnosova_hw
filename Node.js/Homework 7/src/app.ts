import express, { Request, Response } from "express";
import path from "path";
import bodyParser from "body-parser";
import cors from "cors";

class App {
   public app: express.Application;
   public port: string | number;

   constructor(controllers: any, port: string | number) {
      this.app = express();
      this.port = port;

      this.initializeMiddlewares();
      this.initializeRoutes(controllers);
   }

   private initializeMiddlewares() {
      this.app.use(cors());
      this.app.use(bodyParser.json());
      this.app.use(express.static(path.join(__dirname, "../news-frontend/build")));
   }

   private initializeRoutes(controllers: any) {
      this.app.get("/", (req: Request, res: Response) => {
         res.sendFile(path.join(__dirname, "../news-frontend/build", "index.html"));
      });

      controllers.forEach((controller: any) => {
         this.app.use("/", controller.router);
      });
   }

   public listen() {
      this.app.listen(this.port, () => {
         console.log(`Server is running on http://localhost:${this.port}`);
      });
   }
}

export default App;
