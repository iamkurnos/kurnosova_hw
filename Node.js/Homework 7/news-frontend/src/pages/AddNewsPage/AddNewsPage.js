import { useState } from "react";
import "./AddNewsPage.scss";
import { useLocation, useNavigate } from "react-router-dom";
import axios from "axios";

const AddNewsPage = () => {
   const location = useLocation();
   const isForEdit = !!location.state;
   let newsItem;
   if (isForEdit) {
      newsItem = location.state.newsItem;
   }
   const [titleValue, setTitleValue] = useState(`${isForEdit ? newsItem.title : ""}`);
   const [textValue, setTextValue] = useState(`${isForEdit ? newsItem.text : ""}`);
   const navigate = useNavigate();

   const handleSubmit = async (e) => {
      e.preventDefault();
      try {
         if (isForEdit) {
            await axios
               .put(`${process.env.REACT_APP_API_URL}+${newsItem.id}`, { title: titleValue, text: textValue })
               .then((res) => navigate("/"));
         } else {
            await axios
               .post(process.env.REACT_APP_API_URL, { title: titleValue, text: textValue })
               .then((res) => navigate("/"));
         }
      } catch (error) {
         console.error(error);
      }
   };

   return (
      <main className="add-news">
         <nav>
            <img
               onClick={() => {
                  navigate("/");
               }}
               src="./img/home-page.png"
               alt="All news"
            />
         </nav>
         <div className="form-wrap">
            <form>
               <h2 className="form__header">{isForEdit ? "Edit your news" : "Create your news"}</h2>
               <textarea
                  required
                  className="form__title"
                  type="text"
                  placeholder="The news headline"
                  value={titleValue}
                  onChange={(event) => {
                     setTitleValue(event.target.value);
                  }}></textarea>
               <textarea
                  required
                  className="form__text"
                  type="text"
                  placeholder="The text of the news"
                  value={textValue}
                  onChange={(event) => {
                     setTextValue(event.target.value);
                  }}></textarea>
               <button className="form__submit" onClick={handleSubmit}>
                  Publish the news
               </button>
            </form>
         </div>
      </main>
   );
};

export default AddNewsPage;
