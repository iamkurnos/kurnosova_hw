import { Link, useLocation, useNavigate } from "react-router-dom";
import "./NewsPage.scss";

const NewsPage = () => {
   const location = useLocation();
   const newsItem = location.state.newsItem;
   const { title, text } = newsItem;
   const navigate = useNavigate()

   return (
      <main className="news-page">
         <nav>
            <img onClick={()=>{navigate('/')}} src="./img/home-page.png" alt="All news" />
         </nav>
         <div className="news-page__content">
            <Link to={`/addnews`} state={{ newsItem }}>
               <img src="./img/edit.png" alt="Edit news" />
            </Link>
            <h2>{title}</h2>
            <p>{text}</p>
         </div>
      </main>
   );
};

export default NewsPage;
