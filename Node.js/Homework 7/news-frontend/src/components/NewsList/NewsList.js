/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import "./NewsList.scss";

const NewsList = () => {
   const [news, setNews] = useState([]);
   const [isLoading, setIsLoading] = useState(true);
   const [pageNumber, setPageNumber] = useState(0);

   useEffect(() => {
      axios
         .get(`${process.env.REACT_APP_API_URL}?page=${pageNumber}&size=4`)
         .then((response) => {
            setNews((prev) => {
               let newsArray = [...prev];
               newsArray = [...newsArray, ...response.data];
               return newsArray;
            });
            setIsLoading(false);
         })
         .catch((error) => {
            console.log(error);
         });
   }, [pageNumber]);

   const handleScroll = () => {
      if (window.innerHeight + document.documentElement.scrollTop + 10 >= document.documentElement.offsetHeight) {
         setPageNumber((prevPageNumber) => prevPageNumber + 1);
      }
   };

   useEffect(() => {
      setPageNumber(1)
      window.addEventListener("scroll", handleScroll);
      return () => window.removeEventListener("scroll", handleScroll);
   }, []);


   return (
      <>
         {!isLoading && (
            <section>
               {news.map((newsItem) => {
                  const { id, title, text } = newsItem;
                  return (
                     <Link to={`/${id}`} state={{ newsItem }} key={`news-${id}`}>
                        <div className="news-item">
                           <h4>{title}</h4>
                           <p>{text}</p>
                        </div>
                     </Link>
                  );
               })}
            </section>
         )}
      </>
   );
};

export default NewsList;
