const http = require('http');

let requestCount = 0;

const server = http.createServer((req, res) => {
  if (req.url === '/') {
    requestCount += 1;
    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.end(JSON.stringify({ message: 'Request handled successfully', requestCount }));
  } else {
    res.writeHead(404, { 'Content-Type': 'text/plain' });
    res.end('Not Found');
  }
});

const portArg = process.argv.find((arg) => arg.includes('--port='));

const port = portArg ? portArg.split('=')[1] : 3000;

server.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
