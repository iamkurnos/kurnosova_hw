"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.newspostSchemaAJV = void 0;
const fileDB_1 = require("./fileDB");
const newspostSchema = {
    id: Number,
    title: String,
    text: String,
    genre: String,
    isPrivate: Boolean,
    createDate: Date,
};
const newspostSchemaAJV = {
    type: "object",
    properties: {
        id: { type: "number" },
        title: { type: "string", maxLength: 50 },
        text: { type: "string", maxLength: 256 },
        genre: { type: "string", enum: ["Politic", "Business", "Sport", "Other"] },
        isPrivate: { type: "boolean" },
        createDate: { type: "string", format: "date-time" },
    },
    required: ["title", "text", "genre", "isPrivate"],
};
exports.newspostSchemaAJV = newspostSchemaAJV;
function initiateSchemes() {
    fileDB_1.database.registerSchema("newspost", newspostSchema);
}
exports.default = initiateSchemes;
