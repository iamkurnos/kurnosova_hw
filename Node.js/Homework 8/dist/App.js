"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const path_1 = __importDefault(require("path"));
const body_parser_1 = __importDefault(require("body-parser"));
const cors_1 = __importDefault(require("cors"));
const errorHandle_1 = __importDefault(require("./utils/errorHandle"));
const logger_1 = require("./utils/logger");
class App {
    constructor(controllers, port) {
        this.app = (0, express_1.default)();
        this.port = port;
        this.initializeMiddlewares();
        this.initializeRoutes(controllers);
        this.handleErrors();
    }
    initializeMiddlewares() {
        this.app.use((0, cors_1.default)());
        this.app.use(body_parser_1.default.json());
        this.app.use(express_1.default.static(path_1.default.join(__dirname, "../news-frontend/build")));
        this.app.use(logger_1.logRequest);
    }
    initializeRoutes(controllers) {
        this.app.get("/", (req, res) => {
            res.sendFile(path_1.default.join(__dirname, "../news-frontend/build", "index.html"));
        });
        controllers.forEach((controller) => {
            this.app.use("/", controller.router);
        });
    }
    handleErrors() {
        this.app.use(errorHandle_1.default);
    }
    listen() {
        this.app.listen(this.port, () => {
            console.log(`Server is running on http://localhost:${this.port}`);
        });
    }
}
exports.default = App;
