"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NewspostsServiceError = exports.ValidationError = void 0;
const ajv_1 = require("ajv");
class ValidationError extends ajv_1.ValidationError {
    constructor(message, errors) {
        super(errors || []);
        this.name = "Validation Error";
        this.message = message;
    }
}
exports.ValidationError = ValidationError;
class NewspostsServiceError extends Error {
    constructor(message, code) {
        super(message);
        this.name = "NewspostsService Error";
        this.code = code;
        Object.setPrototypeOf(this, NewspostsServiceError.prototype);
    }
}
exports.NewspostsServiceError = NewspostsServiceError;
