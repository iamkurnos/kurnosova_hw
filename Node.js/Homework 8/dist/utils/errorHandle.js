"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Errors_1 = require("./Errors");
const logger_1 = __importDefault(require("./logger"));
const errorHandle = (err, req, res, next) => {
    const isDevelopmentMessage = (process.env.NODE_ENV === "development") ? { stack: err.stack } : "";
    const errorInfo = { error: err.name, message: err.message, ...isDevelopmentMessage };
    if (err instanceof Errors_1.ValidationError) {
        logger_1.default.warn(err.message);
        return res.status(400).json(errorInfo);
    }
    else if (err instanceof Errors_1.NewspostsServiceError) {
        logger_1.default.error(err.message, { stack: err.stack });
        return res.status(err.code).json(errorInfo);
    }
    else {
        logger_1.default.error(err.message, { stack: err.stack });
        return res.status(500).json(errorInfo);
    }
};
exports.default = errorHandle;
