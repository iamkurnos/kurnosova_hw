"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.logRequest = void 0;
const winston_1 = __importDefault(require("winston"));
const logger = winston_1.default.createLogger({
    level: "info",
    format: winston_1.default.format.combine(winston_1.default.format.printf(({ level, message, stack }) => `${level}: ${message} ${stack ? '(' + stack + ")" : ""}`)),
    transports: [
        new winston_1.default.transports.Console(),
        new winston_1.default.transports.File({ filename: "application.log" }),
        new winston_1.default.transports.File({ filename: "error.log", level: "error" }),
        new winston_1.default.transports.File({ filename: "error.log", level: "warn" }),
    ],
});
function logRequest(req, res, next) {
    const methodUrl = `METHOD: ${req.method}, URL: ${req.url}`;
    const requestBody = (Object.keys(req.body).length !== 0) ? `, REQUEST BODY: ${JSON.stringify(req.body)}` : "";
    logger.info(`${methodUrl}${requestBody}`);
    next();
}
exports.logRequest = logRequest;
exports.default = logger;
