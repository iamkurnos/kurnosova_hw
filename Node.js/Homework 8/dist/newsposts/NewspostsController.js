"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const NewspostsService_1 = __importDefault(require("./NewspostsService"));
const express_1 = __importDefault(require("express"));
const typedi_1 = require("typedi");
const ajv_1 = __importDefault(require("ajv"));
const ajv_formats_1 = __importDefault(require("ajv-formats"));
const Schemes_1 = require("../Schemes");
const Errors_1 = require("../utils/Errors");
let NewspostsController = class NewspostsController {
    constructor(service) {
        this.service = service;
        this.path = "/newsposts";
        this.router = express_1.default.Router();
        this.getAll = async (req, res, next) => {
            try {
                const params = req.query;
                if (!params.page || !params.size) {
                    throw new Errors_1.NewspostsServiceError("Incorrect query parameters", 400);
                }
                try {
                    const news = await this.service.getAll(params);
                    res.status(200).json(news);
                }
                catch (error) {
                    throw new Errors_1.NewspostsServiceError("Internal server error", 500);
                }
            }
            catch (error) {
                next(error);
            }
        };
        this.getById = async (req, res, next) => {
            try {
                const postId = +req.params.id;
                const newsPost = await this.service.getById(postId);
                if (!newsPost) {
                    throw new Errors_1.NewspostsServiceError("News post not found", 404);
                }
                try {
                    res.status(200).json(newsPost);
                }
                catch (error) {
                    throw new Errors_1.NewspostsServiceError("Internal server error", 500);
                }
            }
            catch (error) {
                next(error);
            }
        };
        this.create = async (req, res, next) => {
            try {
                const newData = req.body;
                const valid = this.newsValidator(newData);
                if (!valid) {
                    throw new Errors_1.ValidationError(this.newsValidator.errors.map((e) => e.message));
                }
                try {
                    const createdNewsPost = await this.service.create(newData);
                    res.status(200).json(createdNewsPost);
                }
                catch (error) {
                    throw new Errors_1.NewspostsServiceError("Internal server error", 500);
                }
            }
            catch (error) {
                next(error);
            }
        };
        this.update = async (req, res, next) => {
            try {
                const postId = +req.params.id;
                const updatedFields = req.body;
                const valid = this.newsValidator(updatedFields);
                if (!valid) {
                    throw new Errors_1.ValidationError(this.newsValidator.errors.map((e) => e.message));
                }
                const updatedNewsPost = await this.service.update(postId, updatedFields);
                if (!updatedNewsPost) {
                    throw new Errors_1.NewspostsServiceError("News post not found", 404);
                }
                try {
                    res.status(200).json(updatedNewsPost);
                }
                catch (error) {
                    throw new Errors_1.NewspostsServiceError("Internal server error", 500);
                }
            }
            catch (error) {
                next(error);
            }
        };
        this.delete = async (req, res, next) => {
            try {
                const postId = +req.params.id;
                const deletedId = await this.service.delete(postId);
                if (!deletedId) {
                    throw new Errors_1.NewspostsServiceError("News post not found", 404);
                }
                try {
                    res.status(200).json({ message: "News post deleted successfully" });
                }
                catch (error) {
                    throw new Errors_1.NewspostsServiceError("Internal server error", 500);
                }
            }
            catch (error) {
                next(error);
            }
        };
        this.error = async (req, res, next) => {
            const serviceError = new Errors_1.NewspostsServiceError("This is a NewspostsService error", 500);
            next(serviceError);
        };
        this.initializeValidators();
        this.intializeRoutes();
    }
    initializeValidators() {
        const ajv = new ajv_1.default({ allErrors: true });
        (0, ajv_formats_1.default)(ajv);
        this.newsValidator = ajv.compile(Schemes_1.newspostSchemaAJV);
    }
    intializeRoutes() {
        this.router.get(this.path, this.getAll);
        this.router.get(this.path + "/error", this.error);
        this.router.get(this.path + "/:id", this.getById);
        this.router.post(this.path, this.create);
        this.router.put(this.path + "/:id", this.update);
        this.router.delete(this.path + "/:id", this.delete);
    }
};
NewspostsController = __decorate([
    (0, typedi_1.Service)(),
    __metadata("design:paramtypes", [NewspostsService_1.default])
], NewspostsController);
exports.default = NewspostsController;
