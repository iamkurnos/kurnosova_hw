"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const typedi_1 = __importDefault(require("typedi"));
const App_1 = __importDefault(require("./App"));
const NewspostsController_1 = __importDefault(require("./newsposts/NewspostsController"));
const Schemes_1 = __importDefault(require("./Schemes"));
const PORT = process.env.PORT || 8000;
(0, Schemes_1.default)();
const app = new App_1.default([typedi_1.default.get(NewspostsController_1.default)], PORT);
app.listen();
