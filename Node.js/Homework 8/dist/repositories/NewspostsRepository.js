"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const fileDB_1 = require("../fileDB");
const typedi_1 = require("typedi");
let NewspostsRepository = class NewspostsRepository {
    constructor() {
        this.newspostTable = fileDB_1.database.getTable("newspost");
    }
    async getAll(params) {
        const { page, size } = params;
        const startIndex = (page - 1) * size;
        const endIndex = page * size;
        try {
            const rawData = await this.newspostTable.getAll();
            return rawData.reverse().slice(startIndex, endIndex);
            ;
        }
        catch (error) {
            throw error;
        }
    }
    async getById(id) {
        try {
            return await this.newspostTable.getById(id);
        }
        catch (error) {
            throw error;
        }
    }
    async create(data) {
        try {
            return await this.newspostTable.create(data);
        }
        catch (error) {
            throw error;
        }
    }
    async update(id, update) {
        try {
            return await this.newspostTable.update(id, update);
        }
        catch (error) {
            throw error;
        }
    }
    async delete(id) {
        try {
            return await this.newspostTable.delete(id);
        }
        catch (error) {
            throw error;
        }
    }
};
NewspostsRepository = __decorate([
    (0, typedi_1.Service)(),
    __metadata("design:paramtypes", [])
], NewspostsRepository);
exports.default = NewspostsRepository;
