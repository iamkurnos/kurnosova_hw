import "reflect-metadata";
import Container from "typedi";
import App from "./App";
import NewspostsController from "./newsposts/NewspostsController";
import initiateSchemes from "./Schemes";

const PORT = process.env.PORT || 8000;

initiateSchemes()

const app = new App([Container.get(NewspostsController)], PORT);

app.listen();
