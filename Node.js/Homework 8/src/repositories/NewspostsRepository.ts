import { database } from "../fileDB";
import { Service } from "typedi";

@Service()
class NewspostsRepository {
   private newspostTable;
   constructor() {
      this.newspostTable = database.getTable("newspost");
   }

   public async getAll(params: any) {
      const { page, size } = params;
      
      const startIndex = (page - 1) * size;
      const endIndex = page * size;
      
      try {
         const rawData = await this.newspostTable.getAll();
         return rawData.reverse().slice(startIndex, endIndex);;
      } catch (error) {
         throw error;
      }
   }
   public async getById(id: number) {
      try {
         return await this.newspostTable.getById(id);
      } catch (error) {
         throw error;
      }
   }
   public async create(data: object) {
      try {
         return await this.newspostTable.create(data);
      } catch (error) {
         throw error;
      }
   }
   public async update(id: number, update: object) {
      try {
         return await this.newspostTable.update(id, update);
      } catch (error) {
         throw error;
      }
   }
   public async delete(id: number) {
      try {
         return await this.newspostTable.delete(id);
      } catch (error) {
         throw error;
      }
   }
}

export default NewspostsRepository;
