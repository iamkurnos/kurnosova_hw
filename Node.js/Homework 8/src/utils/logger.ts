import winston from "winston";
import { Request, Response, NextFunction } from "express";

const logger = winston.createLogger({
   level: "info",
   format: winston.format.combine(
      winston.format.printf(({ level, message, stack }) => `${level}: ${message} ${stack ? '('+stack+")" : ""}`)
   ),
   transports: [
      new winston.transports.Console(),
      new winston.transports.File({ filename: "application.log" }),
      new winston.transports.File({ filename: "error.log", level: "error" }),
      new winston.transports.File({ filename: "error.log", level: "warn" }),
   ],
});

export function logRequest(req: Request, res: Response, next: NextFunction) {
    const methodUrl = `METHOD: ${req.method}, URL: ${req.url}`;
    const requestBody = (Object.keys(req.body).length !== 0) ? `, REQUEST BODY: ${JSON.stringify(req.body)}` : "";
    logger.info(`${methodUrl}${requestBody}`);
    next();
}
 export default logger