import { ValidationError as AjvValidationError } from "ajv";

class ValidationError extends AjvValidationError {
   constructor(message: string, errors?: AjvValidationError[]) {
      super(errors || []);
      this.name = "Validation Error";
      this.message = message;
   }
}

class NewspostsServiceError extends Error {
   code: number;
   constructor(message: string, code: number) {
      super(message);
      this.name = "NewspostsService Error";
      this.code = code;
      Object.setPrototypeOf(this, NewspostsServiceError.prototype);
   }
}

export {ValidationError, NewspostsServiceError}
