import { Request, Response, NextFunction } from "express";
import { ValidationError, NewspostsServiceError } from "./Errors";
import logger from "./logger";

const errorHandle = (err: Error, req: Request, res: Response, next: NextFunction) => {
    const isDevelopmentMessage = (process.env.NODE_ENV === "development") ? { stack: err.stack } : "";
    const errorInfo = { error: err.name, message: err.message, ...isDevelopmentMessage };
    if (err instanceof ValidationError) {
        logger.warn(err.message);
        return res.status(400).json(errorInfo);
    } else if (err instanceof NewspostsServiceError) {
        logger.error(err.message, { stack: err.stack });
        return res.status(err.code).json(errorInfo);
    } else {
        logger.error(err.message, {stack: err.stack });
        return res.status(500).json(errorInfo);
    }
};

export default errorHandle;
