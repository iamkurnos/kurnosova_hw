import NewspostsRepository from "../repositories/NewspostsRepository";
import { Service } from "typedi";

@Service()
class NewspostsService {
   constructor(private repository: NewspostsRepository) {
   }
   
   public async getAll(params: any) {
      try {
         return await this.repository.getAll(params);
      } catch (error) {
         throw error;
      }
   }

   public async getById(id: number) {
      try {
         return await this.repository.getById(id);
      } catch (error) {
         throw error;
      }
   }

   public async create(data: object) {
      try {
         return await this.repository.create(data);
      } catch (error) {
         throw error;
      }
   }

   public async update(id: number, update: object) {
      try {
         return await this.repository.update(id, update);
      } catch (error) {
         throw error;
      }
   }
   public async delete(id: number) {
      try {
         return await this.repository.delete(id);
      } catch (error) {
         throw error;
      }
   }
}

export default NewspostsService;
