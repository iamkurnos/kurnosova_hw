import NewspostsService from "./NewspostsService";
import express, { Request, Response, NextFunction } from "express";
import { Service } from "typedi";
import Ajv from "ajv";
import addFormats from "ajv-formats";
import { newspostSchemaAJV } from "../Schemes";
import { ValidationError, NewspostsServiceError } from "../utils/Errors";

@Service()
class NewspostsController {
   public path = "/newsposts";
   public router = express.Router();
   private newsValidator;

   constructor(private service: NewspostsService) {
      this.initializeValidators();
      this.intializeRoutes();
   }

   public initializeValidators() {
      const ajv = new Ajv({ allErrors: true });
      addFormats(ajv);
      this.newsValidator = ajv.compile(newspostSchemaAJV);
   }

   public intializeRoutes() {
      this.router.get(this.path, this.getAll);
      this.router.get(this.path + "/error", this.error);
      this.router.get(this.path + "/:id", this.getById);
      this.router.post(this.path, this.create);
      this.router.put(this.path + "/:id", this.update);
      this.router.delete(this.path + "/:id", this.delete);
   }

   getAll = async (req: Request, res: Response, next: NextFunction) => {
      try {
         const params = req.query;
         try {            
            const news = await this.service.getAll(params);
            res.status(200).json(news);
         } catch (error) {
            throw new NewspostsServiceError("Internal server error", 500);
         }
      } catch (error) {
         next(error); 
      }
   };

   getById = async (req: Request, res: Response, next: NextFunction) => {
      try {
         const postId = +req.params.id;
         const newsPost = await this.service.getById(postId);
         if (!newsPost) {
            throw new NewspostsServiceError("News post not found", 404);
         }
         try {
            res.status(200).json(newsPost);
         } catch (error) {
            throw new NewspostsServiceError("Internal server error", 500);
         }
      } catch (error) {
         next(error);
      }
   };

   create = async (req: Request, res: Response, next: NextFunction) => {
      try {
         const newData = req.body;
         const valid = this.newsValidator(newData);
   
         if (!valid) {
            throw new ValidationError(this.newsValidator.errors.map((e) => e.message));
         }
         try {
            const createdNewsPost = await this.service.create(newData);
            res.status(200).json(createdNewsPost);
         } catch (error) {
            throw new NewspostsServiceError("Internal server error", 500);
         }
      } catch (error) {
         next(error);
      }
   };

   update = async (req: Request, res: Response, next: NextFunction) => {
      try {
         const postId = +req.params.id;
         const updatedFields = req.body;
         const valid = this.newsValidator(updatedFields);
         if (!valid) {
            throw new ValidationError(this.newsValidator.errors.map((e) => e.message));
         }
         const updatedNewsPost = await this.service.update(postId, updatedFields);
         if (!updatedNewsPost) {
            throw new NewspostsServiceError("News post not found", 404);
         }
         try {
            res.status(200).json(updatedNewsPost);
         } catch (error) {
            throw new NewspostsServiceError("Internal server error", 500);
         }
      } catch (error) {
         next(error);
      }
   };

   delete = async (req: Request, res: Response, next: NextFunction) => {
      try {
         const postId = +req.params.id;
   
         const deletedId = await this.service.delete(postId);
         if (!deletedId) {
            throw new NewspostsServiceError("News post not found", 404);
         }
         try {
            res.status(200).json({ message: "News post deleted successfully" });
         } catch (error) {
            throw new NewspostsServiceError("Internal server error", 500);
         }
      } catch (error) {
         next(error);
      }
   };

   error = async (req: Request, res: Response, next: NextFunction) => {
      const serviceError = new NewspostsServiceError("This is a NewspostsService error", 500);
      next(serviceError);
   }
}

export default NewspostsController;
