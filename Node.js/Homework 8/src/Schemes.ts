import { database, Schema } from "./fileDB";

const newspostSchema: Schema = {
   id: Number,
   title: String, 
   text: String, 
   genre: String, 
   isPrivate: Boolean,
   createDate: Date,
};

const newspostSchemaAJV = {
   type: "object",
   properties: {
      id: { type: "number" },
      title: { type: "string", maxLength: 50 },
      text: { type: "string", maxLength: 256 },
      genre: { type: "string", enum: ["Politic", "Business", "Sport", "Other"] },
      isPrivate: { type: "boolean" },
      createDate: { type: "string", format: "date-time" },
   },
   required: ["title", "text", "genre", "isPrivate"],
};

export default function initiateSchemes (){
    database.registerSchema("newspost", newspostSchema);
}

export {newspostSchemaAJV}