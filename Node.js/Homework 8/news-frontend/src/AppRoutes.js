import { Routes, Route } from "react-router-dom";
import MainPage from "./pages/MainPage/MainPage";
import NewsPage from "./pages/NewsPage/NewsPage";
import AddNewsPage from "./pages/AddNewsPage/AddNewsPage";

const AppRoutes = () => {
   return (
      <Routes>
         <Route path="/" element={<MainPage />} />
         <Route path="/:newsId" element={<NewsPage/>} />
         <Route path="/addnews" element={<AddNewsPage/>} />
      </Routes>
   );
};

export default AppRoutes;