import { Link, useLocation, useNavigate } from "react-router-dom";
import "./NewsPage.scss";
import { genresObj } from "../../shared/schemes";

const NewsPage = () => {
   const location = useLocation();
   const newsItem = location.state.newsItem;
   const { title, text, genre, createDate } = newsItem;
   const navigate = useNavigate()

   return (
      <main className="news-page">
         <nav>
            <img
               onClick={() => {
                  navigate("/");
               }}
               src="./img/home-page.png"
               alt="All news"
            />
         </nav>
         <div className="news-page__content">
            <Link to={`/addnews`} state={{ newsItem}}>
               <img src="./img/edit.png" alt="Edit news" />
            </Link>
            <p className="news-page__genre">{genresObj[genre]}</p>
            <h2>{title}</h2>
            <p>{text}</p>
            <p className="news-page__date">{new Date(createDate).toLocaleDateString()}</p>
         </div>
      </main>
   );
};

export default NewsPage;
