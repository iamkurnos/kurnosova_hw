import { useState } from "react";
import "./AddNewsPage.scss";
import { useLocation, useNavigate } from "react-router-dom";
import axios from "axios";
import { genresObj } from "../../shared/schemes";

const AddNewsPage = () => {
   const location = useLocation();
   const isForEdit = !!location.state;
   let newsItem;
   if (isForEdit) {
      newsItem = location.state.newsItem;
   }
   const [titleValue, setTitleValue] = useState(`${isForEdit ? newsItem.title : ""}`);
   const [textValue, setTextValue] = useState(`${isForEdit ? newsItem.text : ""}`);
   const [genreValue, setGenreValue] = useState(isForEdit ? newsItem.genre : null);
   const [isPrivate, setIsPrivate] = useState(isForEdit ? newsItem.isPrivate : false);
   const navigate = useNavigate();

   function findKeyByValue(valueToFind) {
      for (let key in genresObj) {
         if (genresObj[key] === valueToFind) {
            return key;
         }
      };
   }

   const handleSubmit = async (e) => {
      e.preventDefault();
      try {
         if (isForEdit) {
            await axios
               .put(`${process.env.REACT_APP_API_URL}+${newsItem.id}`, {
                  title: titleValue,
                  text: textValue,
                  genre: genreValue,
                  isPrivate: isPrivate,
               })
               .then((res) => navigate("/"));
         } else {
            await axios
               .post(process.env.REACT_APP_API_URL, {
                  title: titleValue,
                  text: textValue,
                  genre: genreValue,
                  isPrivate: isPrivate,
               })
               .then((res) => navigate("/"));
         }
      } catch (error) {
         console.error(error);
      }
   };

   return (
      <main className="add-news">
         <nav>
            <img
               onClick={() => {
                  navigate("/");
               }}
               src="./img/home-page.png"
               alt="All news"
            />
         </nav>
         <div className="form-wrap">
            <form>
               <h2 className="form__header">{isForEdit ? "Відкоригуйте новину" : "Створити новину"}</h2>
               <select
                  value={genresObj[genreValue] || "Виберіть тему новини"}
                  onChange={(event) => {
                     setGenreValue(findKeyByValue(event.target.value));
                  }}>
                  <option disabled>Виберіть тему новини</option>
                  {Object.keys(genresObj).map((key) => (
                     <option key={key}>{genresObj[key]}</option>
                  ))}
               </select>
               <textarea
                  required
                  className="form__title"
                  type="text"
                  placeholder="Заголовок новини"
                  value={titleValue}
                  onChange={(event) => {
                     setTitleValue(event.target.value);
                  }}></textarea>
               <textarea
                  required
                  className="form__text"
                  type="text"
                  placeholder="Текст новини"
                  value={textValue}
                  onChange={(event) => {
                     setTextValue(event.target.value);
                  }}></textarea>
               <div className="form__radio">
                  <div
                     className="form__radio-btn"
                     onClick={() => {
                        setIsPrivate(!isPrivate);
                     }}>
                     {isPrivate && <div></div>}
                  </div>
                  <label>Приватна новина</label>
               </div>
               <button className="form__submit" onClick={handleSubmit}>
                  Опублікувати новину
               </button>
            </form>
         </div>
      </main>
   );
};

export default AddNewsPage;
