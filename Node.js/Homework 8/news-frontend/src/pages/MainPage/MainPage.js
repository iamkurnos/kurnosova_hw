import { Link } from "react-router-dom";
import "./MainPage.scss";
import NewsList from "../../components/NewsList/NewsList";

const MainPage = () => {
   return (
      <main className="main-page">
         <Link to={`/addnews`}>
            <button className="add-news-btn">Опублікувати новину</button>
         </Link>
         <NewsList />
      </main>
   );
};

export default MainPage;
