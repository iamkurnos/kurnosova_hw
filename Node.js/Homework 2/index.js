const fileDB = require("./fileDB");

const newspostSchema = {
   id: Number,
   title: String,
   text: String,
   createDate: Date,
};

fileDB.registerSchema("newspost", newspostSchema);

const newspostTable = fileDB.getTable("newspost");

const data1 = {
   title: "У зоопарку Чернігова лисичка народила лисеня",
   text: "В Чернігівському заопарку сталася чудова подія! Лисичка на ім'я Руда народила чудове лисенятко! Тож поспішайте навідатись та подивитись на це миле створіння!",
   createDate: new Date(),
};

const data2 = {
   title: "У Києві не буде масових заходів на День незалежності, — КМДА",
   text: "Як поінформували міську владу листом з Офісу Президента, на державному рівні на Хрещатику планують провести виставку зразків пошкодженої ворожої техніки, —  йдеться у заяві міської адміністрації.",
   createDate: new Date(),
};

(async () => {
   let post1 = await newspostTable.create(data1);
   console.log("Created newspost:", post1);

   let post2 = await newspostTable.create(data2);
   console.log("Created newspost:", post2);

   let getAllPosts = await newspostTable.getAll();
   console.log("All posts:", getAllPosts);

   post1 = await newspostTable.update(post1.id, { title: "Маленька лисичка" });
   console.log("Updated post:", post1);

   post1 = await newspostTable.getById(post1.id);
   console.log("Received post by id:", post1);

   post1 = await newspostTable.delete(post1.id);
   console.log("Deleted newspost ID:", post1);

   getAllPosts = await newspostTable.getAll();
   console.log("All posts:", getAllPosts);

})();
