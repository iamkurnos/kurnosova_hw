const fs = require("fs").promises;

const database = {};

function registerSchema(schemaName, schema) {
   database[schemaName] = {
      schema,
      data: [],
   };
}

function getTable(schemaName) {
   const table = database[schemaName];

   return {
      getAll: async () => {
         await loadData(schemaName);
         return table.data;
      },
      getById: async (id) => {
         await loadData(schemaName);
         return table.data.find((item) => item.id === id);
      },
      create: async (data) => {
         await loadData(schemaName);
         const newData = filterDataBySchema(data, table.schema);
         newData.id = Date.now();
         table.data.push(newData);
         await saveData(schemaName);
         return newData;
      },
      update: async (id, updatedFields) => {
         await loadData(schemaName);
         const itemIndex = table.data.findIndex((item) => item.id === id);
         if (itemIndex === -1) {
            return null;
         }
         const updatedData = filterDataBySchema(updatedFields, table.schema);
         table.data[itemIndex] = { ...table.data[itemIndex], ...updatedData };
         await saveData(schemaName);
         return table.data[itemIndex];
      },
      delete: async (id) => {
         await loadData(schemaName);
         const itemIndex = table.data.findIndex((item) => item.id === id);
         if (itemIndex === -1) {
            return null;
         }
         const deletedId = table.data[itemIndex].id;
         table.data.splice(itemIndex, 1);
         await saveData(schemaName);
         return deletedId;
      },
   };
}

async function loadData(schemaName) {
   try {
      const rawData = await fs.readFile(`${schemaName}.json`, "utf-8");
      database[schemaName].data = JSON.parse(rawData);
   } catch (error) {
      database[schemaName].data = [];
   }
}

async function saveData(schemaName) {
   await fs.writeFile(`${schemaName}.json`, JSON.stringify(database[schemaName].data, null, 2));
}

function filterDataBySchema(data, schema) {
   const filteredData = {};
   for (const field in schema) {
      if (data.hasOwnProperty(field)) {
         filteredData[field] = data[field];
      }
   }
   return filteredData;
}

module.exports = {
   registerSchema,
   getTable,
};
