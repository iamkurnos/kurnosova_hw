import { v4 as uuidv4 } from 'uuid';

enum CurrencyEnum {
  USD = 'USD',
  UAH = 'UAH'
}

class Transaction {
  id: string;
  amount: number;
  currency: CurrencyEnum;

  constructor(amount: number, currency: CurrencyEnum) {
    this.id = uuidv4();
    this.amount = amount;
    this.currency = currency;
  }
}

class Card {
  transactions: Transaction[] = [];

  addTransaction(transactionOrAmount: Transaction | number, currency?: CurrencyEnum): string {
    if (transactionOrAmount instanceof Transaction) {
      this.transactions.push(transactionOrAmount);
      return transactionOrAmount.id;
    } else if (currency !== undefined) {
      const transaction = new Transaction(transactionOrAmount, currency);
      this.transactions.push(transaction);
      return transaction.id;
    } else {
      throw new Error('Invalid parameters');
    }
  }

  getTransaction(id: string): Transaction | undefined {
    return this.transactions.find(transaction => transaction.id === id);
  }

  getBalance(currency: CurrencyEnum): number {
    return this.transactions.reduce((balance, transaction) => {
      if (transaction.currency === currency) {
        return balance + transaction.amount;
      }
      return balance;
    }, 0);
  }
}

const card = new Card();

const transactionId1 = card.addTransaction( 100, CurrencyEnum.USD);
const transactionId2 = card.addTransaction( 500, CurrencyEnum.UAH);
const transactionId3 = card.addTransaction(new Transaction(200, CurrencyEnum.USD));

console.log('Transactions on the card:', card.transactions);
console.log('Transaction with id', transactionId1, ':', card.getTransaction(transactionId1));
console.log('Balance in USD:', card.getBalance(CurrencyEnum.USD));
console.log('Balance in UAH:', card.getBalance(CurrencyEnum.UAH));