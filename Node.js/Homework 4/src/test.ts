import { v4 as uuidv4 } from 'uuid';

enum CurrencyEnum {
  USD = 'USD',
  UAH = 'UAH'
}

interface ICard {
  addTransaction(transaction: Transaction | number, currency?: CurrencyEnum): string;
  getTransaction(id: string): Transaction | undefined;
  getBalance(currency: CurrencyEnum): number;
}

class Transaction {
  id: string;

  constructor(public amount: number, public currency: CurrencyEnum) {
    this.id = uuidv4();
  }
}

class Card implements ICard {
  transactions: Transaction[] = [];

  addTransaction(transactionOrAmount: Transaction | number, currency?: CurrencyEnum): string {
    if (transactionOrAmount instanceof Transaction) {
      this.transactions.push(transactionOrAmount);
      return transactionOrAmount.id;
    } else if (currency !== undefined) {
      const transaction = new Transaction(transactionOrAmount, currency);
      this.transactions.push(transaction);
      return transaction.id;
    } else {
      throw new Error('Invalid parameters');
    }
  }

  getTransaction(id: string): Transaction | undefined {
    return this.transactions.find(transaction => transaction.id === id);
  }

  getBalance(currency: CurrencyEnum): number {
    return this.transactions.reduce((balance, transaction) => {
      if (transaction.currency === currency) {
        return balance + transaction.amount;
      }
      return balance;
    }, 0);
  }
}

class BonusCard extends Card implements ICard {
  addTransaction(transactionOrAmount: Transaction | number, currency?: CurrencyEnum): string {
    if (transactionOrAmount instanceof Transaction) {
      const bonusAmount = transactionOrAmount.amount * 0.1;
      const bonusTransaction = new Transaction(bonusAmount, transactionOrAmount.currency);
      this.transactions.push(transactionOrAmount,bonusTransaction);
      return transactionOrAmount.id;
    } else if (currency !== undefined) {
      const bonusAmount = transactionOrAmount * 0.1;
      const transaction = new Transaction(transactionOrAmount, currency);
      const bonusTransaction = new Transaction(bonusAmount, currency);
      this.transactions.push(transaction, bonusTransaction);
      return transaction.id;
    } else {
      throw new Error('Invalid parameters');
    }
  }
}

class Pocket {
  cards: { [name: string]: ICard } = {};

  addCard(name: string, card: ICard): void {
    if (this.cards[name]) {
      console.log(`Card with name ${name} already exists.`);
    } else {
      this.cards[name] = card;
    }
  }

  getCard(name: string): ICard | undefined {
    return this.cards[name];
  }

  getTotalAmount(currency: CurrencyEnum): number {
    let totalAmount = 0;
    for (const cardName in this.cards) {
      totalAmount += this.cards[cardName].getBalance(currency);
    }
    return totalAmount;
  }
}

const card = new Card();
const bonusCard = new BonusCard();
const pocket = new Pocket();

pocket.addCard('Regular Card', card);
pocket.addCard('Bonus Card', bonusCard);

const transactionId1 = card.addTransaction(100,CurrencyEnum.USD);
const transactionId2 = bonusCard.addTransaction(500,CurrencyEnum.UAH);
const transactionId3 = card.addTransaction(new Transaction(400, CurrencyEnum.USD));
const transactionId4 = bonusCard.addTransaction(new Transaction(200, CurrencyEnum.USD));

console.log('Regular Card Transactions:', card.transactions);
console.log('Bonus Card Transactions:', bonusCard.transactions);
console.log('Transaction with id', transactionId1, ':', card.getTransaction(transactionId1));
console.log('Transaction with id', transactionId2, ':', bonusCard.getTransaction(transactionId2));
console.log('Regular Card Balance in USD:', card.getBalance(CurrencyEnum.USD));
console.log('Bonus Card Balance in UAH:', bonusCard.getBalance(CurrencyEnum.UAH));
console.log('Total Amount in USD:', pocket.getTotalAmount(CurrencyEnum.USD));
console.log('Regular Card Transactions:', pocket.getCard('Regular Card'));
