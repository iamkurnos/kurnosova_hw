import express, { Request, Response } from "express";
import { newspostTable } from "../server";

const router = express.Router();

router.get("/", async (req: Request, res: Response) => {
   try {
      const news = await newspostTable.getAll();
      res.status(200).json(news);
   } catch (error) {
      res.status(500).json({ error: "Internal server error" });
   }
});

router.get("/:id", async (req: Request, res: Response) => {
   const postId = parseInt(req.params.id);
   try {
      const newsPost = await newspostTable.getById(postId);
      if (!newsPost) {
         res.status(404).json({ error: "News post not found" });
         return;
      }
      res.status(200).json(newsPost);
   } catch (error) {
      res.status(500).json({ error: "Internal server error" });
   }
});

router.post("/", async (req: Request, res: Response) => {
   const newData = req.body;

   if (!newData.title || !newData.text) {
      res.status(400).json({ error: "Invalid request body" });
      return;
   }

   try {
      const createdNewsPost = await newspostTable.create(newData);
      res.status(200).json(createdNewsPost);
   } catch (error) {
      res.status(500).json({ error: "Internal server error" });
   }
});

router.put("/:id", async (req: Request, res: Response) => {
   const postId = parseInt(req.params.id);
   const updatedFields = req.body;

   try {
      const updatedNewsPost = await newspostTable.update(postId, updatedFields);
      if (!updatedNewsPost) {
         res.status(404).json({ error: "News post not found" });
         return;
      }
      res.status(200).json(updatedNewsPost);
   } catch (error) {
      res.status(500).json({ error: "Internal server error" });
   }
});

router.delete("/:id", async (req: Request, res: Response) => {
   const postId = parseInt(req.params.id);

   try {
      const deletedId = await newspostTable.delete(postId);
      if (!deletedId) {
         res.status(404).json({ error: "News post not found" });
         return;
      }
      res.status(200).json({ message: "News post deleted successfully" });
   } catch (error) {
      res.status(500).json({ error: "Internal server error" });
   }
});

export default router;
