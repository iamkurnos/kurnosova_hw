import { promises as fs } from "fs";

interface Schema {
  [key: string]: any;
}

class Database {
  private static instance: Database;
  private tables: { [key: string]: Table } = {};

  private constructor() {}

  public static getInstance(): Database {
    if (!Database.instance) {
      Database.instance = new Database();
    }
    return Database.instance;
  }

  public registerSchema(schemaName: string, schema: Schema): void {
    if (!this.tables[schemaName]) {
      this.tables[schemaName] = new Table(schemaName, schema);
    }
  }

  public getTable(schemaName: string): Table {
    return this.tables[schemaName];
  }
}

class Table {
  private data: any[] = [];

  constructor(private schemaName: string, private schema: Schema) {}

  public async getAll(): Promise<any[]> {
    await this.loadData();
    return this.data;
  }

  public async getById(id: number): Promise<any | null> {
    await this.loadData();
    return this.data.find((item) => item.id === id) || null;
  }

  public async create(data: any): Promise<any> {
    await this.loadData();
    const newData = this.filterDataBySchema(data);
    newData.id = Date.now();
    this.data.push(newData);
    await this.saveData();
    return newData;
  }

  public async update(id: number, updatedFields: any): Promise<any | null> {
    await this.loadData();
    const itemIndex = this.data.findIndex((item) => item.id === id);
    if (itemIndex === -1) {
      return null;
    }
    const updatedData = this.filterDataBySchema(updatedFields);
    this.data[itemIndex] = { ...this.data[itemIndex], ...updatedData };
    await this.saveData();
    return this.data[itemIndex];
  }

  public async delete(id: number): Promise<number | null> {
    await this.loadData();
    const itemIndex = this.data.findIndex((item) => item.id === id);
    if (itemIndex === -1) {
      return null;
    }
    const deletedId = this.data[itemIndex].id;
    this.data.splice(itemIndex, 1);
    await this.saveData();
    return deletedId;
  }

  private async loadData(): Promise<void> {
    try {
      const rawData = await fs.readFile(`${this.schemaName}.json`, "utf-8");
      this.data = JSON.parse(rawData);
    } catch (error) {
      this.data = [];
    }
  }

  private async saveData(): Promise<void> {
    await fs.writeFile(`${this.schemaName}.json`, JSON.stringify(this.data, null, 2));
  }

  private filterDataBySchema(data: any): any {
    const filteredData: any = {};
    for (const field in this.schema) {
      if (data.hasOwnProperty(field)) {
        filteredData[field] = data[field];
      }
    }
    return filteredData;
  }
}

export const database = Database.getInstance();
export { Schema};