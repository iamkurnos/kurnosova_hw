import express, { Request, Response } from "express";
import bodyParser from "body-parser";
import { database, Schema } from "./fileDB";
import path from "path";
import cors from "cors";
import newspostRoutes from "./routes/newspostRoutes";

const app = express();
const PORT = process.env.PORT || 8000;

app.use(cors());
app.use(express.static(path.join(__dirname, "../news-frontend/build")));
app.use(bodyParser.json());

const newspostSchema: Schema = {
   title: String,
   text: String,
};

database.registerSchema("newspost", newspostSchema);

let newspostTable = database.getTable("newspost");

app.get("/", (req: Request, res: Response) => {
   res.sendFile(path.join(__dirname, "../news-frontend/build", "index.html"));
});

app.use("/newsposts", newspostRoutes);

app.listen(PORT, () => {
   console.log(`Server is running on http://localhost:${PORT}`);
});

export {newspostTable}