"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.newspostTable = void 0;
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const fileDB_1 = require("./fileDB");
const path_1 = __importDefault(require("path"));
const cors_1 = __importDefault(require("cors"));
const newspostRoutes_1 = __importDefault(require("./routes/newspostRoutes"));
const app = (0, express_1.default)();
const PORT = process.env.PORT || 8000;
app.use((0, cors_1.default)());
app.use(express_1.default.static(path_1.default.join(__dirname, "../news-frontend/build")));
app.use(body_parser_1.default.json());
const newspostSchema = {
    title: String,
    text: String,
};
fileDB_1.database.registerSchema("newspost", newspostSchema);
let newspostTable = fileDB_1.database.getTable("newspost");
exports.newspostTable = newspostTable;
app.get("/", (req, res) => {
    res.sendFile(path_1.default.join(__dirname, "../news-frontend/build", "index.html"));
});
app.use("/newsposts", newspostRoutes_1.default);
app.listen(PORT, () => {
    console.log(`Server is running on http://localhost:${PORT}`);
});
