"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const server_1 = require("../server");
const router = express_1.default.Router();
router.get("/", async (req, res) => {
    try {
        const news = await server_1.newspostTable.getAll();
        res.status(200).json(news);
    }
    catch (error) {
        res.status(500).json({ error: "Internal server error" });
    }
});
router.get("/:id", async (req, res) => {
    const postId = parseInt(req.params.id);
    try {
        const newsPost = await server_1.newspostTable.getById(postId);
        if (!newsPost) {
            res.status(404).json({ error: "News post not found" });
            return;
        }
        res.status(200).json(newsPost);
    }
    catch (error) {
        res.status(500).json({ error: "Internal server error" });
    }
});
router.post("/", async (req, res) => {
    const newData = req.body;
    if (!newData.title || !newData.text) {
        res.status(400).json({ error: "Invalid request body" });
        return;
    }
    try {
        const createdNewsPost = await server_1.newspostTable.create(newData);
        res.status(200).json(createdNewsPost);
    }
    catch (error) {
        res.status(500).json({ error: "Internal server error" });
    }
});
router.put("/:id", async (req, res) => {
    const postId = parseInt(req.params.id);
    const updatedFields = req.body;
    try {
        const updatedNewsPost = await server_1.newspostTable.update(postId, updatedFields);
        if (!updatedNewsPost) {
            res.status(404).json({ error: "News post not found" });
            return;
        }
        res.status(200).json(updatedNewsPost);
    }
    catch (error) {
        res.status(500).json({ error: "Internal server error" });
    }
});
router.delete("/:id", async (req, res) => {
    const postId = parseInt(req.params.id);
    try {
        const deletedId = await server_1.newspostTable.delete(postId);
        if (!deletedId) {
            res.status(404).json({ error: "News post not found" });
            return;
        }
        res.status(200).json({ message: "News post deleted successfully" });
    }
    catch (error) {
        res.status(500).json({ error: "Internal server error" });
    }
});
exports.default = router;
