"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.database = void 0;
const fs_1 = require("fs");
class Database {
    constructor() {
        this.tables = {};
    }
    static getInstance() {
        if (!Database.instance) {
            Database.instance = new Database();
        }
        return Database.instance;
    }
    registerSchema(schemaName, schema) {
        if (!this.tables[schemaName]) {
            this.tables[schemaName] = new Table(schemaName, schema);
        }
    }
    getTable(schemaName) {
        return this.tables[schemaName];
    }
}
class Table {
    constructor(schemaName, schema) {
        this.schemaName = schemaName;
        this.schema = schema;
        this.data = [];
    }
    async getAll() {
        await this.loadData();
        return this.data;
    }
    async getById(id) {
        await this.loadData();
        return this.data.find((item) => item.id === id) || null;
    }
    async create(data) {
        await this.loadData();
        const newData = this.filterDataBySchema(data);
        newData.id = Date.now();
        this.data.push(newData);
        await this.saveData();
        return newData;
    }
    async update(id, updatedFields) {
        await this.loadData();
        const itemIndex = this.data.findIndex((item) => item.id === id);
        if (itemIndex === -1) {
            return null;
        }
        const updatedData = this.filterDataBySchema(updatedFields);
        this.data[itemIndex] = { ...this.data[itemIndex], ...updatedData };
        await this.saveData();
        return this.data[itemIndex];
    }
    async delete(id) {
        await this.loadData();
        const itemIndex = this.data.findIndex((item) => item.id === id);
        if (itemIndex === -1) {
            return null;
        }
        const deletedId = this.data[itemIndex].id;
        this.data.splice(itemIndex, 1);
        await this.saveData();
        return deletedId;
    }
    async loadData() {
        try {
            const rawData = await fs_1.promises.readFile(`${this.schemaName}.json`, "utf-8");
            this.data = JSON.parse(rawData);
        }
        catch (error) {
            this.data = [];
        }
    }
    async saveData() {
        await fs_1.promises.writeFile(`${this.schemaName}.json`, JSON.stringify(this.data, null, 2));
    }
    filterDataBySchema(data) {
        const filteredData = {};
        for (const field in this.schema) {
            if (data.hasOwnProperty(field)) {
                filteredData[field] = data[field];
            }
        }
        return filteredData;
    }
}
exports.database = Database.getInstance();
