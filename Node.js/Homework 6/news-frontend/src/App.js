import React from "react";
import "./App.scss";
import AppRoutes from "./AppRoutes";
import { BrowserRouter } from "react-router-dom";

function App() {
   return (
      <BrowserRouter>
         <div className="App">
            <AppRoutes />
         </div>
      </BrowserRouter>
   );
}

export default App;
