/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import "./NewsList.scss";

const NewsList = () => {
   const [news, setNews] = useState([]);
   const [isLoading, setIsLoading] = useState(true);
   useEffect(() => {
      axios
         .get(process.env.REACT_APP_API_URL)
         .then((response) => {
            setNews(response.data.reverse());
            setIsLoading(false);
         })
         .catch((error) => {
            console.log(error);
         });
   }, []);

   return (
      <>
         {!isLoading && (
            <section>
               {news.map((newsItem) => {
                  const { id, title, text } = newsItem;
                  return (
                     <Link to={`/${id}`} state={{ newsItem }} key={`news-${id}`}>
                        <div className="news-item">
                           <h4>{title}</h4>
                           <p>{text}</p>
                        </div>
                     </Link>
                  );
               })}
            </section>
         )}
      </>
   );
};

export default NewsList;
