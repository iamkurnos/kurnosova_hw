import CardsContainer from "../components/CardsContainer/CardsContainer";
import { useSelector } from "react-redux";

const CatalogPage = () => {
   const cards = useSelector((store) => store.cards.cards);
   return <CardsContainer cards={cards} />;
};

export default CatalogPage;