import CartContainer from "../components/CartContainer/CartContainer";
import { useSelector } from "react-redux";

const CartPage = () => {
   const cart = useSelector((store) => store.cart.cart);
   return (
      <>
         <h3>Cart</h3>
         {cart.length === 0 ? <p>You don't have any items in your cart</p> : <CartContainer />}
      </>
   );
};


export default CartPage;
