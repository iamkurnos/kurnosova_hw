import CardsContainer from "../components/CardsContainer/CardsContainer";
import { useSelector } from "react-redux";

const FavouritePage = () => {
   const favorites = useSelector((store) => store.favourites.favouritesArray);
   const cards = useSelector((store) => store.cards.cards);

   let favouriteCards = [];
   favorites.forEach((item) => {
      cards.forEach((card) => {
         if (card.code === item) {
            favouriteCards.push(card)
         }
      });
   });
   return (
      <>
         <h3>Favourites</h3>
         {favorites.length === 0 ? (
            <p>You don't have any items in your favorites</p>
         ) : (
            <CardsContainer cards={favouriteCards} />
         )}
      </>
   );
};

export default FavouritePage;
