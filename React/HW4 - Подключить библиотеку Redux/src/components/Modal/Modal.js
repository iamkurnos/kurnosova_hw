import './Modal.scss'
import PropTypes from 'prop-types';
import { useDispatch} from "react-redux";
import { closeModalAC } from '../../store/modal/actionCreators';

const Modal = ({ text, action}) => {
    const dispatch = useDispatch();

    const closeModal = () =>{dispatch(closeModalAC())}

    return (
        <div
        className="modal-container"
        onMouseDown={(event) => {
            if (event.target.classList.contains("modal-container")) {
            closeModal()
            }
        }}
        >
        <div className="modal">
            <p>{text}</p>
            {action}
        </div>
        </div>
    );
}

Modal.propTypes = {
    text: PropTypes.string,
    action: PropTypes.object.isRequired,
}

Modal.defaultProps = {
    text: '',
}

export default Modal