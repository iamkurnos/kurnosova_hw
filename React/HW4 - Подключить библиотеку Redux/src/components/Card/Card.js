import "./Card.scss";
import PropTypes from "prop-types";
import Button from "../Button/Button";
import { useDispatch } from "react-redux";
import { openModalAC, setModalAssignmentAC, setModalDataAC } from "../../store/modal/actionCreators";
import { updateFavouritesAC } from '../../store/favourite/actionCreators'
import { updateCardsIsFavouriteAC } from "../../store/cards/actionCreators";

const Card = ({ card}) => {
   const { name, price, picture, code, color, isFavourite } = card;

   const dispatch = useDispatch();

   const openModal = () => {
      dispatch(openModalAC());
   };

   const setModalAssignment = (value) => {
      dispatch(setModalAssignmentAC(value));
   }

   const setModalData = (modalObj) => {
      dispatch(setModalDataAC(modalObj));
   };

   const updateFavourites = (card) => {
      dispatch(updateFavouritesAC(card));
   }

   const updateCardsIsFavourite = (card) => {
      dispatch(updateCardsIsFavouriteAC(card));
   }

   return (
      <>
         <img className="card__picture" src={picture} alt={name} />
         <div className="card__info">
            <h4>{name}</h4>
            <p className="card__code">Vendor code: {code}</p>
            <p className="card__color">COLOR - {color.toUpperCase()}</p>
            <div>
               <p className="card__price">${price}</p>
               <div>
                  <img
                     className="card__favourite"
                     src={isFavourite ? "./img/favourite.png" : "./img/notfavourite.png"}
                     alt="like"
                     onClick={() => {
                        updateCardsIsFavourite(card)
                        updateFavourites(card);
                     }}
                  />
                  <Button
                     backgroundColor="rgb(34, 34, 34)"
                     text="ADD TO CART"
                     onClick={() => {
                        setModalData(card);
                        openModal()
                        setModalAssignment("addToCart");
                     }}
                  />
               </div>
            </div>
         </div>
      </>
   );
};

Card.propTypes = {
   card: PropTypes.object.isRequired,
};

export default Card;
