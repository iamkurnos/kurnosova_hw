import "./CartContainer.scss";
import CartItem from "../CartItem/CartItem";
import { useSelector } from "react-redux";

const CartContainer = () => {
   const cart = useSelector((store) => store.cart.cart);
   const cards = useSelector((store) => store.cards.cards);

   const cardsInCart = []
   cart.forEach(item => {
      cards.forEach(card => {
         if (card.code === item.code) {
            cardsInCart.push({ ...card, amount: item.amount });
         }
      })
   })

   return (
         <ul className="cart-container">
            {cardsInCart.map((card) => {
               const { code } = card;
               return (
                  <li className="cart-item" key={code}>
                     <CartItem card={card}/>
                  </li>
               );
            })}
         </ul>
   );
};

export default CartContainer;