import "./CartItem.scss";
import PropTypes from "prop-types";
import Button from "../Button/Button";
import { useDispatch } from "react-redux";
import { openModalAC, setModalAssignmentAC, setModalDataAC } from "../../store/modal/actionCreators";
import { controlCartAC } from "../../store/cart/actionCreators";

const CartItem = ({ card}) => {
   const { name, price, picture, amount } = card;

   const dispatch = useDispatch();
   
   const openModal = () => {
      dispatch(openModalAC());
   };

   const setModalAssignment = (value) => {
      dispatch(setModalAssignmentAC(value));
   };

   const setModalData = (modalObj) => {
      dispatch(setModalDataAC(modalObj));
   };

   const controlCart = (cartObj) => {
      dispatch(controlCartAC(cartObj));
   };

   return (
      <>
         <img src={picture} alt={name} />
         <div className="cart-item__info">
            <div className="cart-item__header">
               <h4>{name}</h4>
               <p>
                  $<span>{(price * amount).toFixed(2)}</span>
               </p>
            </div>
            <div className="cart-item__control">
               <Button
                  backgroundColor="white"
                  text="-"
                  onClick={() => {
                     controlCart({ ...card, operation: "decrease" });
                  }}
               />
               <div>{amount}</div>
               <Button
                  backgroundColor="white"
                  text="+"
                  onClick={() => {
                     controlCart({ ...card, operation: "increase" });
                  }}
               />
               <img
                  src="./img/delete.png"
                  alt="delete"
                  onClick={() => {
                     setModalData(card);
                     openModal()
                     setModalAssignment("delete");
                  }}
               />
            </div>
         </div>
      </>
   );
};

CartItem.propTypes = {
   card: PropTypes.object.isRequired,
};

export default CartItem;
