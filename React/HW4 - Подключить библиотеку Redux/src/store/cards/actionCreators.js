import { UPDATE_CARDS_IS_FAVOURITE, GET_CARDS } from "./actions";

export const updateCardsIsFavouriteAC = (payload) => ({ type: UPDATE_CARDS_IS_FAVOURITE, payload });

export const getCardsAC = () => async (dispatch) => {
   let cards = await fetch("./data.json").then((res) => res.json());
   const favArray = JSON.parse(localStorage.getItem("favourites"));
   if (favArray) {
      cards = cards.map((card) => {
         if (favArray.includes(card.code)) {
            card.isFavourite = true;
         }
         return card;
      });
   }
   dispatch({ type: GET_CARDS, payload: cards });
};
