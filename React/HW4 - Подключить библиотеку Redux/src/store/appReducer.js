import { combineReducers } from "redux";
import modalReducer from "./modal/modalReducer";
import favouriteReducer from "./favourite/favouriteReducer";
import cardsReducer from "./cards/cardsReducer";
import cartReducer from "./cart/cartReducer";

const appReducer = combineReducers({
   modal: modalReducer,
   favourites: favouriteReducer,
   cards: cardsReducer,
   cart: cartReducer,
});

export default appReducer;