import { OPEN_MODAL, CLOSE_MODAL, SET_MODAL_DATA, SET_MODAL_ASSIGNMENT } from "./actions";

const initialState = {
   isModalOpen: false,
   assignment: "",
   modalData: {},
};

const modalReducer = (state = initialState, action) => {
   switch (action.type) {
      case OPEN_MODAL: {
         return { ...state, isModalOpen: true };
      }

      case CLOSE_MODAL: {
         return { ...state, isModalOpen: false };
      }

      case SET_MODAL_DATA: {
         return { ...state, modalData: action.payload };
      }

      case SET_MODAL_ASSIGNMENT: {
         return { ...state, assignment: action.payload };
      }

      default: {
         return state;
      }
   }
};

export default modalReducer;
