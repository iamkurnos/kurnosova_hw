import { GET_CART, CONTROL_CART } from "./actions";

const initialState = {
   cart: [],
};

const cartReducer = (state = initialState, action) => {
   switch (action.type) {
       case GET_CART: {
         return { ...state, cart: action.payload };
      }
      case CONTROL_CART: {
         const cartArray = [...state.cart];
         const cartCodes = cartArray.map(({ code }) => code);
         const index = cartCodes.indexOf(action.payload.code);
         if (index === -1) {
            cartArray.push({ code: action.payload.code, amount: 1 });
         } else {
            if (action.payload.operation === "increase") {
               cartArray[index].amount++;
            } else if (action.payload.operation === "decrease") {
               cartArray[index].amount--;
               if (cartArray[index].amount < 1) {
                  cartArray.splice(index, 1);
               }
            } else if (action.payload.operation === "delete") {
               cartArray.splice(index, 1);
            }
         }
         localStorage.setItem("cart", JSON.stringify(cartArray));
         return { ...state, cart: cartArray };
      }
      default: {
         return state;
      }
   }
};

export default cartReducer;
