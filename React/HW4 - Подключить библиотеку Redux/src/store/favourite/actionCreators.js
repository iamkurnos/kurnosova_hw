import { UPDATE_FAVOURITES, GET_FAVOURITES } from "./actions";

export const updateFavouritesAC = (payload) => ({ type: UPDATE_FAVOURITES, payload: payload.code });

export const getFavouritesAC = () => (dispatch) => {
    const favouritesInLS = JSON.parse(localStorage.getItem("favourites"));
    if (favouritesInLS) {
        dispatch({ type: GET_FAVOURITES, payload: favouritesInLS });
    } ;
} ;