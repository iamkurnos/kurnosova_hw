import { UPDATE_FAVOURITES, GET_FAVOURITES } from "./actions";

const initialState = {
   favouritesArray: [],
};

const favouriteReducer = (state = initialState, action) => {
   switch (action.type) {
      case GET_FAVOURITES: {
         return { ...state, favouritesArray: action.payload };
      }
      case UPDATE_FAVOURITES: {
         const favArray = [...state.favouritesArray];
         const index = favArray.indexOf(action.payload);
         if (index === -1) {
            favArray.push(action.payload);
         } else {
            favArray.splice(index, 1);
         }
         localStorage.setItem("favourites", JSON.stringify(favArray));
         return { ...state, favouritesArray: favArray };
      }
      default: {
         return state;
      }
   }
};

export default favouriteReducer;
