import { useEffect } from "react";
import "./App.scss";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
import Header from "./components/Header/Header";
import { BrowserRouter } from "react-router-dom";
import AppRoutes from "./AppRoutes";
import { useDispatch, useSelector} from "react-redux";
import { closeModalAC } from "./store/modal/actionCreators";
import { getFavouritesAC} from "./store/favourite/actionCreators";
import { getCardsAC} from "./store/cards/actionCreators";
import { getCartAC, controlCartAC } from "./store/cart/actionCreators";

const App = () => {

   const isModalOpen = useSelector((store) => store.modal.isModalOpen);
   const assignment = useSelector((store) => store.modal.assignment);
   const modalData = useSelector((store) => store.modal.modalData);

   const dispatch = useDispatch();

   const closeModal = () => {
      dispatch(closeModalAC());
   };

   const controlCart = (cartObj) => {
      dispatch(controlCartAC(cartObj));
   }

   useEffect(() => {
      dispatch(getCardsAC());
      dispatch(getFavouritesAC());
      dispatch(getCartAC());
   }, [dispatch]);

   return (
      <BrowserRouter>
         <div className="App">
            <Header />
            
            <main>
               <AppRoutes/>
            </main>

            {isModalOpen && assignment === "addToCart" && (
               <Modal
                  text={"Do you want to add " + modalData.name + " to your shopping cart?"}
                  action={
                     <div className="modal__buttons">
                        <Button
                           text="OK"
                           onClick={() => {
                              controlCart({ ...modalData, operation: "increase" });
                              closeModal();
                           }}
                        />
                        <Button text="Cancel" onClick={closeModal} />
                     </div>
                  }
               />
            )}

            {isModalOpen && assignment === "delete" && (
               <Modal
                  text={"Do you want to delete " + modalData.name + " from your shopping cart?"}
                  action={
                     <div className="modal__buttons">
                        <Button
                           text="OK"
                           onClick={() => {
                              controlCart({ ...modalData, operation: "delete" });
                              closeModal();
                           }}
                        />
                        <Button text="Cancel" onClick={closeModal} />
                     </div>
                  }
               />
            )}
         </div>
      </BrowserRouter>
   );
};

export default App;
