import CartContainer from "../components/CartContainer/CartContainer";
import FormForOrder from "../components/FormForOrder/FormForOrder";
import { useSelector } from "react-redux";


const CartPage = () => {
   const cart = useSelector((store) => store.cart.cart);
   const cards = useSelector((store) => store.cards.cards);

   const cardsInCart = [];
   cart.forEach((item) => {
      cards.forEach((card) => {
         if (card.code === item.code) {
            cardsInCart.push({ ...card, amount: item.amount });
         }
      });
   });
   
   return (
      <>
         <h3>Cart</h3>
         {cart.length === 0 ? (
            <p>You don't have any items in your cart</p>
         ) : (
            <div className="cart-page">
               <CartContainer cardsInCart={cardsInCart} />
               <FormForOrder cardsInCart={cardsInCart} />
            </div>
         )}
      </>
   );
};

export default CartPage;
