import { UPDATE_CARDS_IS_FAVOURITE, GET_CARDS } from "./actions";

const initialState = {
   cards: [],
};

const cardsReducer = (state = initialState, action) => {
   switch (action.type) {
      case GET_CARDS: {
         return { ...state, cards: action.payload };
      }
      case UPDATE_CARDS_IS_FAVOURITE: {
         let cards = [...state.cards];
         cards = cards.map((elem) => {
            if (elem.code === action.payload.code) {
               elem.isFavourite = !action.payload.isFavourite;
            }
            return elem;
         });
         return { ...state, cards: cards };
      }
      default: {
         return state;
      }
   }
};

export default cardsReducer;
