import { GET_CART, CONTROL_CART, CLEAN_CART } from "./actions";

export const controlCartAC = (payload) => ({ type: CONTROL_CART, payload: payload });

export const getCartAC = () => (dispatch) => {
   const cartArray = JSON.parse(localStorage.getItem("cart"));
   if (cartArray) {
      dispatch({ type: GET_CART, payload: cartArray });
   }
};

export const cleanCartAC = (payload) => ({ type: CLEAN_CART, payload: payload });
