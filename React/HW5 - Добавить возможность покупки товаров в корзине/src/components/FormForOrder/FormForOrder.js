import "./FormForOrder.scss";
import React from "react";
import { Formik, Form, Field } from "formik";
import * as yup from "yup";
import { useDispatch } from "react-redux";
import { cleanCartAC } from "../../store/cart/actionCreators";
import PropTypes from "prop-types";
import Button from "../Button/Button";
import InputMask from "react-input-mask";

const FormForOrder = ({ cardsInCart }) => {
   const dispatch = useDispatch();

   const cartString = cardsInCart
      .map(({ name, amount, price }) => {
         return name + ": qt. " + amount + ", total cost $" + amount * price;
      })
      .join("\n");

   const initialValues = {
      name: "",
      surname: "",
      age: "",
      address: "",
      phone: "",
   };

   const validationSchema = yup.object().shape({
      name: yup
         .string()
         .min(2, "The name is too short!")
         .max(50, "The name is too long!")
         .matches(/^[A-Za-z ]*$/, "The name should not contain digits")
         .required("Name field is required"),
      surname: yup
         .string()
         .min(2, "The surname is too short!")
         .max(50, "The surname is too long!")
         .matches(/^[A-Za-z ]*$/, "The surname should not contain digits")
         .required("Surname field is required"),
      age: yup
         .number()
         .integer("The age should be a whole number!")
         .min(18, "The age should be at least 18!")
         .max(100, "The age should not exceed 100!")
         .required("Age field is required"),
      address: yup
         .string()
         .min(5, "The address is too short!")
         .max(100, "The address is too long!")
         .required("Delivery address field is required"),
      phone: yup
         .string()
         .matches(/^\(\d{3}\)\d{3}-\d{2}-\d{2}$/, "Wrong phone format")
         .required("Required"),
   });

   return (
      <Formik
         initialValues={initialValues}
         validationSchema={validationSchema}
         onSubmit={(values, { resetForm }) => {
            console.log("Your order:\n" + cartString + "\nOrder data:\n" + Object.values(values).join(" "));
            dispatch(cleanCartAC());
            resetForm();
         }}>
           {({ touched, errors, isValid, values}) => {
               const disabled =
                  Object.keys(touched).length !== Object.keys(values).length && Object.values(values).includes("");
            return (
               <Form>
                  <h4>Enter your data to send the order:</h4>
                  <label>
                     Name:
                     <Field type="text" name="name" />
                     {touched.name && errors.name && <span className="error">{errors.name}</span>}
                  </label>
                  <label>
                     Surname:
                     <Field type="text" name="surname" />
                     {touched.surname && errors.surname && <span className="error">{errors.surname}</span>}
                  </label>
                  <label>
                     Age:
                     <Field type="text" name="age" />
                     {touched.age && errors.age && <span className="error">{errors.age}</span>}
                  </label>
                  <label>
                     Delivery address:
                     <Field type="text" name="address" />
                     {touched.address && errors.address && <span className="error">{errors.address}</span>}
                  </label>
                  <label>
                     Phone:
                     <Field name="phone">{({ field }) => <InputMask {...field} mask="(999)999-99-99" />}</Field>
                     {touched.phone && errors.phone && <span className="error">{errors.phone}</span>}
                  </label>
                  <Button disabled={disabled ? true : !isValid} type="submit" text="Checkout" />
               </Form>
            );
         }}
      </Formik>
   );
};

FormForOrder.propTypes = {
   cardsInCart: PropTypes.array.isRequired,
};

export default FormForOrder;
