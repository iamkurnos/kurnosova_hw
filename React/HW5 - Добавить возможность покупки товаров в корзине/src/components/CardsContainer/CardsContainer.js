import "./CardsContainer.scss";
import PropTypes from "prop-types";
import Card from "../Card/Card";

const CardsContainer = ({ cards }) => {
   return (
      <ul className="card-container">
       {cards.map(card => {
         const {code} = card
         return(
            <li className="card" key={code}>
               <Card card={card}/>
            </li>
         )})}
      </ul>
   );
};

CardsContainer.propTypes = {
  cards: PropTypes.array.isRequired,
};

export default CardsContainer;
