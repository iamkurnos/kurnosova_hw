import "./Button.scss";
import PropTypes from "prop-types";

const Button = ({ text, onClick, type, disabled}) => {
   return (
      <button
         type={type}
         className={disabled ? "btn-disabled" : "btn"}
         onClick={() => {
            onClick();
         }}>
         {text}
      </button>
   );
};

Button.propTypes = {
   backgroundColor: PropTypes.string,
   text: PropTypes.string.isRequired,
   onClick: PropTypes.func,
   type: PropTypes.string,
   disabled: PropTypes.bool,
};

Button.defaultProps = {
   type: "button",
   onClick: () => { },
   disabled: false,
};

export default Button;
