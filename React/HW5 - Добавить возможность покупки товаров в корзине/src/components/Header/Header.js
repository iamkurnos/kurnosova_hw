import "./Header.scss";
import { NavLink } from "react-router-dom";
import { useSelector } from "react-redux";

const Header = () => {
   const cart = useSelector((store) => store.cart.cart);
   const favorites = useSelector((store) => store.favourites.favouritesArray);

   let cartCounter = 0;
   cart.forEach(({ amount }) => (cartCounter += amount));
   return (
      <header>
         <h2>SHOE STORE</h2>
         <nav>
            <NavLink className="catalog-link" to="/">
               Catalog
            </NavLink>
            <div className="header-icons">
               <NavLink className="icon icon--favourive" to="/favourite">
                  <img src="./img/notfavourite.png" alt="like counter" />
                  {favorites.length !== 0 && favorites !== null && <span>{favorites.length}</span>}
               </NavLink>
               <NavLink className="icon icon--cart" to="/cart">
                  <img src="./img/cart.png" alt="like counter" />
                  {+cartCounter !== 0 && cartCounter !== null && <span>{cartCounter}</span>}
               </NavLink>
            </div>
         </nav>
      </header>
   );
};

export default Header;
