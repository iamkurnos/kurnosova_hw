import { useState, useEffect } from "react";
import "./App.scss";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
import Header from "./components/Header/Header";
import { BrowserRouter } from "react-router-dom";
import AppRoutes from "./AppRoutes";

const App = () => {
   const [activeModal, setActiveModal] = useState({ status: false });
   const [cards, setCards] = useState([]);
   const [modalProps, setModalProps] = useState({});
   const [cardsInCart, setCardsInCart] = useState([]);
   const [favouriteCards, setfavouriteCards] = useState([]);

   useEffect(() => {
      const getData = async () => {
         let cards = await fetch("./data.json").then((res) => res.json());
         const favArray = JSON.parse(localStorage.getItem("favouriteCards"));
         if (favArray) {
            cards = cards.map((card) => {
               if (favArray.includes(card.code)) {
                  card.isFavourite = true;
               }
               return card;
            });
            setfavouriteCards(favArray);
         }
         const cartArray = JSON.parse(localStorage.getItem("cardsInCart"));
         if (cartArray) {
            setCardsInCart(cartArray);
         }
         setCards(cards);
      };
      getData();
   }, []);

   const handleClick = (funcModal) => {
      setActiveModal((prev) => {
         const status = !prev.status;
         const value = funcModal;
         return { status: status, value: value };
      });
   };

   const controlCardinCart = (card, action) => {
      setCardsInCart((prev) => {
         const cartArray = [...prev];
         const cartCodes = cartArray.map(({ code }) => code);
         const index = cartCodes.indexOf(card.code);
         if (index === -1) {
            cartArray.push({ code: card.code, amount: 1 });
         } else {
            if (action === "increase") {
               cartArray[index].amount++;
            } else if (action === "decrease") {
               cartArray[index].amount--;
               if (cartArray[index].amount < 1) {
                  cartArray.splice(index, 1);
               }
            } else if (action === "delete") {
               cartArray.splice(index, 1);
            }
         }
         localStorage.setItem("cardsInCart", JSON.stringify(cartArray));
         return cartArray;
      });
   };

   const setCardFavourite = (card) => {
      setCards((prev) => {
         let cards = [...prev];
         cards = cards.map((elem) => {
            if (elem.code === card.code) {
               elem.isFavourite = !card.isFavourite;
            }
            return elem;
         });
         return cards;
      });

      setfavouriteCards((prev) => {
         const favArray = [...prev];
         const index = favArray.indexOf(card.code);
         if (index === -1) {
            favArray.push(card.code);
         } else {
            favArray.splice(index, 1);
         }
         localStorage.setItem("favouriteCards", JSON.stringify(favArray));
         return favArray;
      });
   };

   return (
      <BrowserRouter>
         <div className="App">
            <Header cardsInCart={cardsInCart} favouriteCards={favouriteCards} />

            <main>
               <AppRoutes
                  setModalProps={setModalProps}
                  setCardFavourite={setCardFavourite}
                  cards={cards}
                  favouriteCards={favouriteCards}
                  handleClick={handleClick}
                  cardsInCart={cardsInCart}
                  controlCardinCart={controlCardinCart}
               />
            </main>

            {activeModal.status && activeModal.value === "addToCart" && (
               <Modal
                  onMouseDown={() => {
                     handleClick("addToCart");
                  }}
                  text={"Do you want to add " + modalProps.name + " to your shopping cart?"}
                  action={
                     <div className="modal__buttons">
                        <Button
                           text="OK"
                           onClick={() => {
                              controlCardinCart(modalProps, "increase");
                              handleClick("addToCart");
                           }}
                        />
                        <Button
                           text="Cancel"
                           onClick={() => {
                              handleClick("addToCart");
                           }}
                        />
                     </div>
                  }
               />
            )}

            {activeModal.status && activeModal.value === "delete" && (
               <Modal
                  onMouseDown={() => {
                     handleClick("delete");
                  }}
                  text={"Do you want to delete " + modalProps.name + " from your shopping cart?"}
                  action={
                     <div className="modal__buttons">
                        <Button
                           text="OK"
                           onClick={() => {
                              controlCardinCart(modalProps, "delete");
                              handleClick("delete");
                           }}
                        />
                        <Button
                           text="Cancel"
                           onClick={() => {
                              handleClick("delete");
                           }}
                        />
                     </div>
                  }
               />
            )}
         </div>
      </BrowserRouter>
   );
};

export default App;
