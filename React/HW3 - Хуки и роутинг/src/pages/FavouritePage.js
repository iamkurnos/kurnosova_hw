import CardsContainer from "../components/CardsContainer/CardsContainer";
import PropTypes from "prop-types";

const FavouritePage = ({ cards, favouriteCards, handleClick, setModalProps, setCardFavourite }) => {

   let favourites = [];
   favouriteCards.forEach((item) => {
      cards.forEach((card) => {
         if (card.code === item) {
            favourites.push(card);
         }
      });
   });
   return (
      <>
         <h3>Favourites</h3>
         {favouriteCards.length === 0 ? (
            <p>You don't have any items in your favorites</p>
         ) : (
            <CardsContainer
               setModalProps={setModalProps}
               setCardFavourite={setCardFavourite}
               cards={favourites}
               handleClick={handleClick}
            />
         )}
      </>
   );
};

FavouritePage.propTypes = {
   cards: PropTypes.array.isRequired,
   favouriteCards: PropTypes.array.isRequired,
   handleClick: PropTypes.func.isRequired,
   setModalProps: PropTypes.func.isRequired,
   setCardFavourite: PropTypes.func.isRequired,
};

export default FavouritePage;
