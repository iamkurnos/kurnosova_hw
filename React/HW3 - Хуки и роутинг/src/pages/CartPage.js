import CartContainer from "../components/CartContainer/CartContainer";
import PropTypes from "prop-types";

const CartPage = ({ cardsInCart, cards, controlCardinCart, handleClick, setModalProps }) => {
   return (
      <>
         <h3>Cart</h3>
         {cardsInCart.length === 0 ? (
            <p>You don't have any items in your cart</p>
         ) : (
            <CartContainer
               cardsInCart={cardsInCart}
               controlCardinCart={controlCardinCart}
               handleClick={handleClick}
               setModalProps={setModalProps}
               cards={cards}
            />
         )}
      </>
   );
};

CartPage.propTypes = {
   handleClick: PropTypes.func.isRequired,
   setModalProps: PropTypes.func.isRequired,
   cardsInCart: PropTypes.array.isRequired,
   cards: PropTypes.array.isRequired,
   controlCardinCart: PropTypes.func.isRequired,
};

export default CartPage;
