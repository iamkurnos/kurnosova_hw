import CardsContainer from "../components/CardsContainer/CardsContainer";
import PropTypes from "prop-types";

const CatalogPage = ({ cards, handleClick, setModalProps, setCardFavourite }) => {
   return (
      <CardsContainer
         setModalProps={setModalProps}
         setCardFavourite={setCardFavourite}
         cards={cards}
         handleClick={handleClick}
      />
   );
};

CatalogPage.propTypes = {
   cards: PropTypes.array.isRequired,
   handleClick: PropTypes.func.isRequired,
   setModalProps: PropTypes.func.isRequired,
   setCardFavourite: PropTypes.func.isRequired,
};

export default CatalogPage;
