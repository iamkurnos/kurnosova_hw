import { Routes, Route } from "react-router-dom";
import CatalogPage from "./pages/CatalogPage";
import CartPage from "./pages/CartPage";
import FavouritePage from "./pages/FavouritePage";
import PropTypes from "prop-types";

const AppRoutes = ({
   cards,
   favouriteCards,
   handleClick,
   setModalProps,
   setCardFavourite,
   cardsInCart,
   controlCardinCart,
}) => {
   return (
      <Routes>
         <Route
            path="/"
            element={
               <CatalogPage
                  setModalProps={setModalProps}
                  setCardFavourite={setCardFavourite}
                  cards={cards}
                  handleClick={handleClick}
               />
            }
         />
         <Route
            path="/cart"
            element={
               <CartPage
                  cardsInCart={cardsInCart}
                  controlCardinCart={controlCardinCart}
                  handleClick={handleClick}
                  setModalProps={setModalProps}
                  cards={cards}
               />
            }
         />
         <Route
            path="/favourite"
            element={
               <FavouritePage
                  setModalProps={setModalProps}
                  setCardFavourite={setCardFavourite}
                  favouriteCards={favouriteCards}
                  cards={cards}
                  handleClick={handleClick}
               />
            }
         />
      </Routes>
   );
};

AppRoutes.propTypes = {
   cards: PropTypes.array.isRequired,
   favouriteCards: PropTypes.array.isRequired,
   handleClick: PropTypes.func.isRequired,
   setModalProps: PropTypes.func.isRequired,
   setCardFavourite: PropTypes.func.isRequired,
   cardsInCart: PropTypes.array.isRequired,
   controlCardinCart: PropTypes.func.isRequired,
};

export default AppRoutes;
