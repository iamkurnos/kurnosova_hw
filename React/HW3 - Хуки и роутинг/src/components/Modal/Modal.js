import "./Modal.scss";
import PropTypes from "prop-types";

const Modal = ({ text, action, onMouseDown }) => {
   return (
      <div
         className="modal-container"
         onMouseDown={(event) => {
            if (event.target.classList.contains("modal-container")) {
               onMouseDown();
            }
         }}>
         <div className="modal">
            <p>{text}</p>
            {action}
         </div>
      </div>
   );
};

Modal.propTypes = {
   text: PropTypes.string,
   action: PropTypes.object.isRequired,
   onMouseDown: PropTypes.func.isRequired,
};

Modal.defaultProps = {
   text: "",
};

export default Modal;
