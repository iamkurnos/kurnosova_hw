import "./CardsContainer.scss";
import PropTypes from "prop-types";
import Card from "../Card/Card";

const CardsContainer = ({ cards, handleClick, setModalProps, setCardFavourite }) => {
   return (
      <ul className="card-container">
         {cards.map((card) => {
            const { code } = card;
            return (
               <li className="card" key={code}>
                  <Card
                     card={card}
                     handleClick={handleClick}
                     setModalProps={setModalProps}
                     setCardFavourite={setCardFavourite}
                  />
               </li>
            );
         })}
      </ul>
   );
};

CardsContainer.propTypes = {
   cards: PropTypes.array.isRequired,
   handleClick: PropTypes.func.isRequired,
   setModalProps: PropTypes.func.isRequired,
   setCardFavourite: PropTypes.func.isRequired,
};

export default CardsContainer;
