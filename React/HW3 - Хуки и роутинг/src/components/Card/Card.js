import "./Card.scss";
import PropTypes from "prop-types";
import Button from "../Button/Button";

const Card = ({ card, handleClick, setModalProps, setCardFavourite }) => {
   const { name, price, picture, code, color, isFavourite } = card;
   return (
      <>
         <img className="card__picture" src={picture} alt={name} />
         <div className="card__info">
            <h4>{name}</h4>
            <p className="card__code">Vendor code: {code}</p>
            <p className="card__color">COLOR - {color.toUpperCase()}</p>
            <div>
               <p className="card__price">${price}</p>
               <div>
                  <img
                     className="card__favourite"
                     src={isFavourite ? "./img/favourite.png" : "./img/notfavourite.png"}
                     alt="like"
                     onClick={() => {
                        setCardFavourite(card);
                     }}
                  />
                  <Button
                     backgroundColor="rgb(34, 34, 34)"
                     text="ADD TO CART"
                     onClick={() => {
                        setModalProps(card);
                        handleClick("addToCart");
                     }}
                  />
               </div>
            </div>
         </div>
      </>
   );
};

Card.propTypes = {
   card: PropTypes.object.isRequired,
   handleClick: PropTypes.func.isRequired,
   setModalProps: PropTypes.func.isRequired,
   setCardFavourite: PropTypes.func.isRequired,
};

export default Card;
