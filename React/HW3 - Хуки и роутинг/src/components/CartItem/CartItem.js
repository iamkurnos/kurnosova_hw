import "./CartItem.scss";
import PropTypes from "prop-types";
import Button from "../Button/Button";

const CartItem = ({ card, controlCardinCart, setModalProps, handleClick }) => {
   const { name, price, picture, amount } = card;
   return (
      <>
         <img src={picture} alt={name} />
         <div className="cart-item__info">
            <div className="cart-item__header">
               <h4>{name}</h4>
               <p>
                  $<span>{(price * amount).toFixed(2)}</span>
               </p>
            </div>
            <div className="cart-item__control">
               <Button
                  backgroundColor="white"
                  text="-"
                  onClick={() => {
                     controlCardinCart(card, "decrease");
                  }}
               />
               <div>{amount}</div>
               <Button
                  backgroundColor="white"
                  text="+"
                  onClick={() => {
                     controlCardinCart(card, "increase");
                  }}
               />
               <img
                  src="./img/delete.png"
                  alt="delete"
                  onClick={() => {
                     setModalProps(card);
                     handleClick("delete");
                  }}
               />
            </div>
         </div>
      </>
   );
};

CartItem.propTypes = {
   card: PropTypes.object.isRequired,
   handleClick: PropTypes.func.isRequired,
   setModalProps: PropTypes.func.isRequired,
   controlCardinCart: PropTypes.func.isRequired,
};

export default CartItem;
