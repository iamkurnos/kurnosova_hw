import "./CartContainer.scss";
import PropTypes from "prop-types";
import CartItem from "../CartItem/CartItem";

const CartContainer = ({ cardsInCart, controlCardinCart, setModalProps, handleClick, cards }) => {
   const cartArray = [];
   cardsInCart.forEach((item) => {
      cards.forEach((card) => {
         if (card.code === item.code) {
            cartArray.push({ ...card, amount: item.amount });
         }
      });
   });
   return (
      <ul className="cart-container">
         {cartArray.map((card) => {
            const { code } = card;
            return (
               <li className="cart-item" key={code}>
                  <CartItem
                     card={card}
                     controlCardinCart={controlCardinCart}
                     handleClick={handleClick}
                     setModalProps={setModalProps}
                  />
               </li>
            );
         })}
      </ul>
   );
};

CartContainer.propTypes = {
   cardsInCart: PropTypes.array.isRequired,
   cards: PropTypes.array.isRequired,
   handleClick: PropTypes.func.isRequired,
   setModalProps: PropTypes.func.isRequired,
   controlCardinCart: PropTypes.func.isRequired,
};

export default CartContainer;
