import "./Header.scss";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";

const Header = ({ cardsInCart, favouriteCards }) => {
   let cartCounter = 0;
   cardsInCart.forEach(({ amount }) => (cartCounter += amount));
   return (
      <header>
         <h2>SHOE STORE</h2>
         <nav>
            <NavLink className="catalog-link" to="/">
               Catalog
            </NavLink>
            <div className="header-icons">
               <NavLink className="icon icon--favourive" to="/favourite">
                  <img src="./img/notfavourite.png" alt="like counter" />
                  {favouriteCards.length !== 0 && favouriteCards !== null && <span>{favouriteCards.length}</span>}
               </NavLink>
               <NavLink className="icon icon--cart" to="/cart">
                  <img src="./img/cart.png" alt="like counter" />
                  {+cartCounter !== 0 && cartCounter !== null && <span>{cartCounter}</span>}
               </NavLink>
            </div>
         </nav>
      </header>
   );
};

Header.propTypes = {
   cardsInCart: PropTypes.array,
   favouriteCards: PropTypes.array,
};

Header.defaultProps = {
   cardsInCart: [],
   favouriteCards: [],
};

export default Header;
