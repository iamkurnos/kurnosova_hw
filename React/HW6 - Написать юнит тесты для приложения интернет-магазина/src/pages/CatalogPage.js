import CardsContainer from "../components/CardsContainer/CardsContainer";
import Table from "../components/Table/Table";
import { useSelector } from "react-redux";
import { useContext } from "react";
import { ViewModeContext } from "../components/ViewModeProvider/ViewModeProvider";

const CatalogPage = () => {
   const cards = useSelector((store) => store.cards.cards);
   const { viewMode, setViewMode } = useContext(ViewModeContext);
   return (
      <>
         <div className="toggle-view">
            <img
               className="toggle-view__table"
               onClick={() => {
                  setViewMode("table");
               }}
               src="./img/table-view.png"
               alt="table-view"
            />
            <img
               className="toggle-view__cards"
               onClick={() => {
                  setViewMode("cards");
               }}
               src="./img/cards-view.png"
               alt="cards-view"
            />
         </div>
         {viewMode === "cards" && <CardsContainer cards={cards} />}
         {viewMode === "table" && <Table cards={cards} />}
      </>
   );
};

export default CatalogPage;
