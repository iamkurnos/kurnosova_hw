import { createSlice } from "@reduxjs/toolkit";

const cardsSlice = createSlice({
   name: "cards",
   initialState: {
      cards: [],
   },
   reducers: {
      getCards: (state, action) => {
         state.cards = action.payload;
      },
      updateCardsIsFavourite: (state, action) => {
         let cards = [...state.cards];
         cards = cards.map((elem) => {
            if (elem.code === action.payload.code) {
               elem.isFavourite = !action.payload.isFavourite;
            }
            return elem;
         });
         state.cards = cards
      },
   },
});

const { getCards, updateCardsIsFavourite } = cardsSlice.actions

const getCardsAsync = () => async (dispatch) => {
   let cards = await fetch("./data.json").then((res) => res.json());
   const favArray = JSON.parse(localStorage.getItem("favourites"));
   if (favArray) {
      cards = cards.map((card) => {
         if (favArray.includes(card.code)) {
            card.isFavourite = true;
         }
         return card;
      });
   }
   dispatch(getCards(cards));
}

export {getCards, getCardsAsync, updateCardsIsFavourite };

export default cardsSlice.reducer;
