import { createSlice } from "@reduxjs/toolkit";

const getCartFromLS = () => {
   const cartArray = localStorage.getItem("cart")
   return cartArray ? JSON.parse(cartArray) : [];
};

const cartSlice = createSlice({
   name: "cart",
   initialState: {
      cart: getCartFromLS(),
   },
   reducers: {
      controlCart: (state, action) => {
         const cartArray = [...state.cart];
         const cartCodes = cartArray.map(({ code }) => code);
         const index = cartCodes.indexOf(action.payload.code);
         if (index === -1) {
            cartArray.push({ code: action.payload.code, amount: 1 });
         } else {
            if (action.payload.operation === "increase") {
               cartArray[index].amount++;
            } else if (action.payload.operation === "decrease") {
               cartArray[index].amount--;
               if (cartArray[index].amount < 1) {
                  cartArray.splice(index, 1);
               }
            } else if (action.payload.operation === "delete") {
               cartArray.splice(index, 1);
            }
         }
         localStorage.setItem("cart", JSON.stringify(cartArray));
         state.cart = cartArray;
      },
      cleanCart: (state) => {
         localStorage.removeItem("cart");
         state.cart = [];
      },
   },
});

export const { controlCart, cleanCart } = cartSlice.actions

export default cartSlice.reducer;
