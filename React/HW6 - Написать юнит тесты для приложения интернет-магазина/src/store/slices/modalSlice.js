import { createSlice } from "@reduxjs/toolkit";

const modalSlice = createSlice({
   name: "modal",
   initialState: {
      isModalOpen: false,
      assignment: "",
      modalData: {},
   },
   reducers: {
      openModal: (state) => {
         state.isModalOpen = true;
      },
      closeModal: (state) => {
         state.isModalOpen = false;
      },
      setModalData: (state, action) => {
         state.modalData = action.payload;
      },
      setModalAssignment: (state, action) => {
         state.assignment = action.payload;;
      }
   },
});

export const{openModal, closeModal, setModalData, setModalAssignment} = modalSlice.actions

export default modalSlice.reducer;
