import modalReducer, { openModal, closeModal, setModalData, setModalAssignment } from "./modalSlice";

describe("modalSlice reducer", () => {
   test("should handle openModal", () => {
      const initialState = {
         isModalOpen: false,
         assignment: "",
         modalData: {},
      };
      const action = openModal();
      const newState = modalReducer(initialState, action);
      expect(newState.isModalOpen).toBe(true);
   });

   test("should handle closeModal", () => {
      const initialState = {
         isModalOpen: true,
         assignment: "",
         modalData: {},
      };
      const action = closeModal();
      const newState = modalReducer(initialState, action);
      expect(newState.isModalOpen).toBe(false);
   });

   test("should handle setModalData", () => {
      const initialState = {
         isModalOpen: false,
         assignment: "",
         modalData: {},
      };
      const data = { name: "John Doe" };
      const action = setModalData(data);
      const newState = modalReducer(initialState, action);
      expect(newState.modalData).toEqual(data);
   });

   test("should handle setModalAssignment", () => {
      const initialState = {
         isModalOpen: false,
         assignment: "",
         modalData: {},
      };
      const assignment = "Task 1";
      const action = setModalAssignment(assignment);
      const newState = modalReducer(initialState, action);
      expect(newState.assignment).toEqual(assignment);
   });
});
