import favouriteReducer, { updateFavourites } from "./favouriteSlice";

describe("favouriteSlice reducer", () => {
   test("should handle updateFavourites with a new code", () => {
      const initialState = { favouritesArray: ["A"] };
      const action = updateFavourites({ code: "B" });
      const newState = favouriteReducer(initialState, action);
      expect(newState.favouritesArray).toEqual(["A", "B"]);
   });

   test("should handle updateFavourites with an existing code", () => {
      const initialState = { favouritesArray: ["A", "B"] };
      const action = updateFavourites({ code: "B" });
      const newState = favouriteReducer(initialState, action);
      expect(newState.favouritesArray).toEqual(["A"]);
   });
});
