import cardsReducer, { getCards, updateCardsIsFavourite } from './cardsSlice'

describe("cards reducer", () => {
   it("should handle getCards", () => {
      const initialState = {
         cards: [],
      };

      const nextState = cardsReducer(initialState, getCards([{ code: "1", isFavourite: false }]));
      expect(nextState.cards).toEqual([{ code: "1", isFavourite: false }]);
   });

   it("should handle updateCardsIsFavourite", () => {
      const initialState = {
         cards: [
            { code: "1", isFavourite: false },
            { code: "2", isFavourite: true },
         ],
      };
      
      const nextState = cardsReducer(initialState, updateCardsIsFavourite({ code: "1", isFavourite: false }));

      expect(nextState.cards).toEqual([
         { code: "1", isFavourite: true },
         { code: "2", isFavourite: true },
      ]);
   });
});
