import cartReducer, { controlCart, cleanCart } from "./cartSlice";

describe("cartSlice reducer", () => {
   const initialState = {
      cart: [
         { code: "A", amount: 2 },
         { code: "B", amount: 1 },
      ],
   };

   test("should handle controlCart with increase operation", () => {
      expect(cartReducer(initialState, controlCart({ code: "A", operation: "increase" }))).toEqual({
         cart: [
            { code: "A", amount: 3 },
            { code: "B", amount: 1 },
         ],
      });
   });
   test("should handle controlCart with decrease operation", () => {
      expect(cartReducer(initialState, controlCart({ code: "A", operation: "decrease" }))).toEqual({
         cart: [
            { code: "A", amount: 1 },
            { code: "B", amount: 1 },
         ],
      });
   });
    test("should handle controlCart with decrease operation to 0", () => {
       expect(cartReducer(initialState, controlCart({ code: "B", operation: "decrease" }))).toEqual({
          cart: [
             { code: "A", amount: 2 },
          ],
       });
    });
   test("should handle controlCart with delete operation", () => {
      expect(cartReducer(initialState, controlCart({ code: "A", operation: "delete" }))).toEqual({
         cart: [{ code: "B", amount: 1 }],
      });
   });

   test("should handle controlCart with new item", () => {
      const initialState = { cart: [] };
      expect(cartReducer(initialState, controlCart({ code: "A", operation: "increase" }))).toEqual({
         cart: [{ code: "A", amount: 1 }],
      });
   });

   test("should handle cleanCart", () => {
      expect(cartReducer(initialState, cleanCart())).toEqual({
         cart: [],
      });
   });
});
