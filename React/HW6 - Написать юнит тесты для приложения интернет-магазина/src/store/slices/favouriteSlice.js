import { createSlice } from "@reduxjs/toolkit";

const getFavouritesFromLS = () => {
   const favouritesInLS = localStorage.getItem("favourites");
   return favouritesInLS ? JSON.parse(favouritesInLS) : [];
};


const favouriteSlice = createSlice({
   name: "favourites",
   initialState: {
      favouritesArray: getFavouritesFromLS(),
   },
   reducers: {
      updateFavourites: (state, action) => {
         const favArray = [...state.favouritesArray];
         const index = favArray.indexOf(action.payload.code);
         if (index === -1) {
            favArray.push(action.payload.code);
         } else {
            favArray.splice(index, 1);
         }
         localStorage.setItem("favourites", JSON.stringify(favArray));
         state.favouritesArray = favArray;
      },
   },
});

export const { updateFavourites} = favouriteSlice.actions

export default favouriteSlice.reducer;
