import { configureStore } from "@reduxjs/toolkit";
import cardsReducer from './slices/cardsSlice'
import cartReducer from './slices/cartSlice'
import favouriteReducer from "./slices/favouriteSlice";
import modalReducer from "./slices/modalSlice";

export const store = configureStore({
   reducer: {
      cards: cardsReducer,
      cart: cartReducer,
      favourites: favouriteReducer,
      modal: modalReducer,
   },
});