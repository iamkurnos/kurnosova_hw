import "./CartItem.scss";
import PropTypes from "prop-types";
import Button from "../Button/Button";
import { useDispatch } from "react-redux";
import { openModal, setModalAssignment, setModalData } from "../../store/slices/modalSlice";
import { controlCart } from "../../store/slices/cartSlice";

const CartItem = ({ card}) => {
   const { name, price, picture, amount } = card;

   const dispatch = useDispatch();

   return (
      <>
         <img src={picture} alt={name} />
         <div className="cart-item__info">
            <div className="cart-item__header">
               <h4>{name}</h4>
               <p>
                  $<span>{(price * amount).toFixed(2)}</span>
               </p>
            </div>
            <div className="cart-item__control">
               <Button
                  backgroundColor="white"
                  text="-"
                  onClick={() => {
                     dispatch(controlCart({ ...card, operation: "decrease" }));
                  }}
               />
               <div>{amount}</div>
               <Button
                  backgroundColor="white"
                  text="+"
                  onClick={() => {
                     dispatch(controlCart({ ...card, operation: "increase" }));
                  }}
               />
               <img
                  src="./img/delete.png"
                  alt="delete"
                  onClick={() => {
                     dispatch(setModalData(card));
                     dispatch(openModal())
                     dispatch(setModalAssignment("delete"));
                  }}
               />
            </div>
         </div>
      </>
   );
};

CartItem.propTypes = {
   card: PropTypes.object.isRequired,
};

export default CartItem;
