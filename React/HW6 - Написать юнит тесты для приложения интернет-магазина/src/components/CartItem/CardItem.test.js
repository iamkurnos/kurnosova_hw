import { render } from "@testing-library/react";
import { store } from "../../store";
import { Provider } from "react-redux";
import CartItem from "./CartItem";

describe("CartItem snapshot testing", () => {
   const card = {
      name: "shoes",
      price: "100",
      picture: "picture",
      amount: 1,
   };
   test("should CartItem match snapshot", () => {
      const { asFragment } = render(
         <Provider store={store}>
            <CartItem card={card} />
         </Provider>
      );
      expect(asFragment()).toMatchSnapshot();
   });
});
