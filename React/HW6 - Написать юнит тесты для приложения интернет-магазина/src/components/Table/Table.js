import PropTypes from "prop-types";
import TableItem from "../TableItem/TableItem";
import './Table.scss'

const Table = ({ cards }) => {
   return (
            <table className="table-container">
               <thead>
                  <tr>
                     <th>Image</th>
                     <th>Product Name</th>
                     <th>Product Code</th>
                     <th>Color</th>
                     <th>Price</th>
                     <th>Actions</th>
                  </tr>
               </thead>
               <tbody>
                  {cards.map((card) => {
                     const { code } = card;
                     return (
                        <tr className="table-line" key={code}>
                           <TableItem card={card} />
                        </tr>
                     );
                  })}
               </tbody>
            </table>
         )

};

Table.propTypes = {
   cards: PropTypes.array.isRequired,
};

export default Table;
