import PropTypes from "prop-types";
import ItemActions from "../ItemActions/ItemActions";
import './TableItem.scss'

const TableItem = ({ card }) => {
   const { name, price, picture, code, color, isFavourite } = card;

   return (
      <>
         <td className="img-cell">
            <img className="item-img" src={picture} alt={name} />
         </td>
         <td>{name}</td>
         <td>{code}</td>
         <td>{color.toUpperCase()}</td>
         <td>${price}</td>
         <td className="actions-cell">
            <ItemActions isFavourite={isFavourite} card={card} />
         </td>
      </>
   );
};

TableItem.propTypes = {
   card: PropTypes.object.isRequired,
};

export default TableItem;
