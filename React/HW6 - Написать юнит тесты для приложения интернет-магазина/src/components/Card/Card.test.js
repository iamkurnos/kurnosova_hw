import { render } from "@testing-library/react";
import {store} from '../../store'
import Card from './Card'
import { Provider } from "react-redux";

describe("Card snapshot testing", () => {
   const card = {
      name: "shoes",
      price: "100",
      picture: "picture",
      code: '00',
      color: 'white',
      isFavourite: false,
   };
   test("should Card match snapshot", () => {
       const { asFragment } = render(
          <Provider store={store}>
             <Card card={card} />
          </Provider>
       );
      expect(asFragment()).toMatchSnapshot();
   });
});
