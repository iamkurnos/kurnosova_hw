import "./Card.scss";
import PropTypes from "prop-types";
import ItemActions from "../ItemActions/ItemActions";

const Card = ({ card }) => {
   const { name, price, picture, code, color, isFavourite } = card;

   return (
      <>
         <img className="card__picture" src={picture} alt={name} />
         <div className="card__info">
            <h4>{name}</h4>
            <p className="card__code">Vendor code: {code}</p>
            <p className="card__color">COLOR - {color.toUpperCase()}</p>
            <div>
               <p className="card__price">${price}</p>
               <div>
                  <ItemActions isFavourite={isFavourite} card={card}/>
               </div>
            </div>
         </div>
      </>
   );
};

Card.propTypes = {
   card: PropTypes.object.isRequired,
};

export default Card;
