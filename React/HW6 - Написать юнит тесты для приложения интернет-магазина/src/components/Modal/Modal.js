import "./Modal.scss";
import PropTypes from "prop-types";
import { useDispatch } from "react-redux";
import { closeModal } from "../../store/slices/modalSlice";

const Modal = ({ text, action }) => {
   const dispatch = useDispatch();

   return (
      <div
         data-testid="modal-container"
         className="modal-container"
         onMouseDown={(event) => {
            if (event.target.classList.contains("modal-container")) {
               dispatch(closeModal());
            }
         }}>
         <div className="modal">
            <p>{text}</p>
            {action}
         </div>
      </div>
   );
};

Modal.propTypes = {
   text: PropTypes.string,
   action: PropTypes.object.isRequired,
};

Modal.defaultProps = {
   text: "",
};

export default Modal;
