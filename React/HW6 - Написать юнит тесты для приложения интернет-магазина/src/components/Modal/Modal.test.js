import { render, screen, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { useDispatch } from "react-redux";
import Modal from "./Modal";
import { closeModal } from "../../store/slices/modalSlice";


describe("Modal snapshot testing", () => {
   test("should Modal match snapshot", () => {
      const { asFragment } = render(<Modal text="This is a modal" action={<button>Close</button>} />);
      expect(asFragment()).toMatchSnapshot();
   });
});

jest.mock("react-redux", () => ({
   useDispatch: jest.fn(),
}));

describe("Modal component", () => {

   const text = "This is a modal";
   const action = <button>Close</button>

   test("should render modal with text and action", () => {
      render(<Modal text={text} action={action} />);

      expect(screen.getByText(text)).toBeInTheDocument();
      expect(screen.getByText("Close")).toBeInTheDocument();
   });

   test("should dispatch closeModal when clicking outside the modal", () => {
      const dispatchMock = jest.fn();
      useDispatch.mockReturnValue(dispatchMock);

      render(<Modal text={text} action={action} />);

      const modalContainer = screen.getByTestId("modal-container");
      fireEvent.mouseDown(modalContainer);

      expect(dispatchMock).toHaveBeenCalledWith(closeModal());
   });
});
