import "./CartContainer.scss";
import CartItem from "../CartItem/CartItem";
import PropTypes from "prop-types";

const CartContainer = ({ cardsInCart }) => {

   return (
      <ul className="cart-container">
         {cardsInCart.map((card) => {
            const { code } = card;
            return (
               <li className="cart-item" key={code}>
                  <CartItem card={card} />
               </li>
            );
         })}
      </ul>
   );
};

CartContainer.propTypes = {
   cardsInCart: PropTypes.array.isRequired,
}

export default CartContainer;