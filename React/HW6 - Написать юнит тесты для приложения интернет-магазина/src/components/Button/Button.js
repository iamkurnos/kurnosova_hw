import "./Button.scss";
import PropTypes from "prop-types";

const Button = ({ text, onClick, type, disabled, className}) => {
   return (
      <button
         type={type}
         className={disabled ? "btn-disabled" : className}
         onClick={() => {
            onClick();
         }}>
         {text}
      </button>
   );
};

Button.propTypes = {
   backgroundColor: PropTypes.string,
   text: PropTypes.string.isRequired,
   onClick: PropTypes.func,
   type: PropTypes.string,
   disabled: PropTypes.bool,
   className: PropTypes.string,
};

Button.defaultProps = {
   type: "button",
   onClick: () => { },
   disabled: false,
   className: 'btn',
};

export default Button;
