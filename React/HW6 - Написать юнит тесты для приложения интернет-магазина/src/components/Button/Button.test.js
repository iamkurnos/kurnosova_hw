import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import Button from "./Button";


describe('Button snapshot testing', () => {
   test("should Button match snapshot", () => {
      const { asFragment } = render(<Button text="Click me" />);
      expect(asFragment()).toMatchSnapshot();
   });
})

describe("Button", () => {
   test("should button render with provided text", () => {
      render(<Button text="Click me" />);
      expect(screen.getByText("Click me")).toBeInTheDocument();
   });

   test("should onClick function call when clicked", () => {
      const onClickMock = jest.fn();
      render(<Button text="Click me" onClick={onClickMock} />);
      const buttonElement = screen.getByText("Click me");
      fireEvent.click(buttonElement);
      expect(onClickMock).toHaveBeenCalled();
   });

   test("should button render with provided type", () => {
      render(<Button text="Submit" type="submit" />);
      const buttonElement = screen.getByRole("button");
      expect(buttonElement.getAttribute("type")).toBe("submit");
   });

   test("should disabled button render when disabled prop is true", () => {
      render(<Button text="Disabled" disabled />);
      const buttonElement = screen.getByRole("button");
      expect(buttonElement.classList.contains("btn-disabled")).toBe(true);
   });

   test("should enable button render when disabled prop is false", () => {
      render(<Button text="Enabled" disabled={false} />);
      const buttonElement = screen.getByRole("button");
      expect(buttonElement.classList.contains("btn")).toBe(true);
   });

   test("has default props if not provided", () => {
      render(<Button text="Default" />);
      const buttonElement = screen.getByRole("button");
      expect(buttonElement.getAttribute("type")).toBe("button");
      expect(buttonElement.classList.contains("btn")).toBe(true);
   });
});
