import './ItemAction.scss'
import PropTypes from "prop-types";
import Button from "../Button/Button";
import { useDispatch } from "react-redux";
import { openModal, setModalData, setModalAssignment } from "../../store/slices/modalSlice";
import { updateFavourites } from "../../store/slices/favouriteSlice";
import { updateCardsIsFavourite } from "../../store/slices/cardsSlice";

const ItemActions = ({ card, isFavourite }) => {

   const dispatch = useDispatch();

   return (
      <>
         <img
            className="icon-favourite"
            src={isFavourite ? "./img/favourite.png" : "./img/notfavourite.png"}
            alt="like"
            onClick={() => {
               dispatch(updateCardsIsFavourite(card));
               dispatch(updateFavourites(card));
            }}
         />
           <Button
            className='btn btn--add'
            backgroundColor="rgb(34, 34, 34)"
            text="ADD TO CART"
            onClick={() => {
               dispatch(setModalData(card));
               dispatch(openModal());
               dispatch(setModalAssignment("addToCart"));
            }}
         />
      </>
   );
};

ItemActions.propTypes = {
   card: PropTypes.object.isRequired,
   isFavourite: PropTypes.bool
};

ItemActions.defaultProp = {
    isFavourite: false,
};

export default ItemActions;
