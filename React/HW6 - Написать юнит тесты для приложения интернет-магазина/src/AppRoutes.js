import { Routes, Route} from "react-router-dom";
import CatalogPage from "./pages/CatalogPage";
import CartPage from "./pages/CartPage";
import FavouritePage from "./pages/FavouritePage";

const AppRoutes = () => {
   return (
      <Routes>
         <Route path="/" element={<CatalogPage/>} />
         <Route path="/cart" element={<CartPage/>} />
         <Route path="/favourite" element={<FavouritePage/>} />
      </Routes>
   );
};

export default AppRoutes;
