import { useEffect } from "react";
import "./App.scss";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
import Header from "./components/Header/Header";
import { BrowserRouter } from "react-router-dom";
import AppRoutes from "./AppRoutes";
import { useDispatch, useSelector } from "react-redux";
import { closeModal } from "./store/slices/modalSlice";
import { getCardsAsync } from "./store/slices/cardsSlice";
import { controlCart } from "./store/slices/cartSlice";
import { ViewModeProvider } from "./components/ViewModeProvider/ViewModeProvider";

const App = () => {
   const isModalOpen = useSelector((store) => store.modal.isModalOpen);
   const assignment = useSelector((store) => store.modal.assignment);
   const modalData = useSelector((store) => store.modal.modalData);

   const dispatch = useDispatch();

   const closeModalFunc = () => {
      dispatch(closeModal());
   };

   useEffect(() => {
      dispatch(getCardsAsync());
   }, [dispatch]);

   return (
      <BrowserRouter>
         <div className="App">
            <Header />

            <main>
               <ViewModeProvider>
                  <AppRoutes />
               </ViewModeProvider>
            </main>

            {isModalOpen && assignment === "addToCart" && (
               <Modal
                  text={"Do you want to add " + modalData.name + " to your shopping cart?"}
                  action={
                     <div className="modal__buttons">
                        <Button
                           text="OK"
                           onClick={() => {
                              dispatch(controlCart({ ...modalData, operation: "increase" }));
                              closeModalFunc();
                           }}
                        />
                        <Button text="Cancel" onClick={closeModalFunc} />
                     </div>
                  }
               />
            )}

            {isModalOpen && assignment === "delete" && (
               <Modal
                  text={"Do you want to delete " + modalData.name + " from your shopping cart?"}
                  action={
                     <div className="modal__buttons">
                        <Button
                           text="OK"
                           onClick={() => {
                              dispatch(controlCart({ ...modalData, operation: "delete" }));
                              closeModalFunc();
                           }}
                        />
                        <Button text="Cancel" onClick={closeModalFunc} />
                     </div>
                  }
               />
            )}
         </div>
      </BrowserRouter>
   );
};

export default App;
