import { PureComponent } from "react";
import './Header.scss'
import PropTypes from 'prop-types';

class Header extends PureComponent{
    render() {
        const { cartCounter, favouriteCardsCodes } = this.props;
        
        return (
          <header>
            <h2>SHOE STORE</h2>
            <div className="header-icons">
              <div>
                <img src="./img/notfavourite.png" alt="like counter" />
                {favouriteCardsCodes.length !== 0 &&
                  favouriteCardsCodes !== null && (
                    <span>{favouriteCardsCodes.length}</span>
                  )}
              </div>
              <div>
                <img src="./img/cart.png" alt="like counter" />
                {+cartCounter !== 0 && cartCounter !== null && (
                  <span>{cartCounter}</span>
                )}
              </div>
            </div>
          </header>
        );
    }
}

Header.propTypes = {
  cartCounter: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  favouriteCardsCodes: PropTypes.array,
};

Header.defaultProps = {
  cartCounter: 0,
  favouriteCardsCodes: [],
};

export default Header