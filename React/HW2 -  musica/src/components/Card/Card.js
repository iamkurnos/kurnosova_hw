import { PureComponent } from 'react';
import './Card.scss'
import PropTypes from 'prop-types';
import Button from '../Button/Button'

class Card extends PureComponent{

    render() {
        let { name, price, picture, code, color, isFavourite, handleClick, setModalProps, setCardFavourite} = this.props

        return (
            <>
                <img className='card__picture' src={picture} alt={name} />
                <div className='card__info'>
                    <h4>{name}</h4>
                    <p className='card__code'>Vendor code: {code}</p>
                    <p className='card__color'>COLOR - {color.toUpperCase()}</p>
                    <div>
                        <p className='card__price'>${price}</p>
                        <div>
                            <img className='card__favourite' src={isFavourite ? './img/favourite.png' : './img/notfavourite.png'} alt="like" onClick={() => {
                                setCardFavourite(code)
                            }} />
                            <Button backgroundColor='rgb(34, 34, 34)' text='ADD TO CART' onClick={() => {
                                setModalProps({code, name})
                                handleClick()
                            }} />
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

Card.propTypes = {
    name: PropTypes.string,
    price: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    picture: PropTypes.string,
    code: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]).isRequired,
    color: PropTypes.string,
    isFavourite: PropTypes.bool,
    handleClick: PropTypes.func.isRequired,
    setModalProps: PropTypes.func.isRequired,
    setCardFavourite: PropTypes.func.isRequired,
}

Card.defaultProps = {
    name: 'Shoes',
    price: '0',
    picture: './img/shoes.png',
    color: 'unknown',
    isFavourite: false
}

export default Card