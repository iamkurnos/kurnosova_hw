import { PureComponent } from "react";
import "./CardsContainer.scss";
import PropTypes from "prop-types";
import Card from "../Card/Card";

class CardsContainer extends PureComponent {
  render() {
    const { cards, handleClick, setModalProps, setCardFavourite} = this.props;
    return (
      <ul className="card-container">
        {cards.map(({ name, price, picture, code, color, isFavourite }) => (
          <li className='card' key={code}>
            <Card
              name={name}
              price={price}
              picture={picture}
              code={code}
              color={color}
              isFavourite={isFavourite}
              handleClick={handleClick}    
              setModalProps={setModalProps}
              setCardFavourite={setCardFavourite}
            />
          </li>
        ))}
      </ul>
    );
  }
}

CardsContainer.propTypes = {
  cards: PropTypes.array.isRequired,
  handleClick: PropTypes.func.isRequired,
  setModalProps: PropTypes.func.isRequired,
  setCardFavourite: PropTypes.func.isRequired,
};

export default CardsContainer;
