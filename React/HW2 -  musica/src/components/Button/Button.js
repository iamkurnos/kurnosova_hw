import { Component } from "react";
import "./Button.scss";
import PropTypes from 'prop-types';

class Button extends Component {
  render() {
    const { backgroundColor, text, onClick } = this.props;
    return (
      <button
        className="btn-open-modal"
        style={{ backgroundColor: backgroundColor }}
        onClick={() => {
          onClick();
        }}
      >
        {text}
      </button>
    );
  }
}

Button.propTypes = {
    backgroundColor: PropTypes.string,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
}

Button.defaultProps = {
    backgroundColor: 'rgb(137, 120, 114)',
}

export default Button;
