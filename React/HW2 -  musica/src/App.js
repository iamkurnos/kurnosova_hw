import React, {Component} from 'react';
import './App.scss';
import Button from './components/Button/Button'
import Modal from './components/Modal/Modal';
import CardContainer from './components/CardsContainer/CardsContainer';
import Header from './components/Header/Header';

class App extends Component {
  state = {
    isActiveModal: false,
    cards: [],
    modalProps: {},
    cartCounter: 0,
    favouriteCardsCodes: [],
  }

  async componentDidMount() {
    let cards = await fetch('./data.json').then(res => res.json())
    const favArray = JSON.parse(localStorage.getItem('favouriteCardsCodes'))
    if (favArray) {
      cards = cards.map((card) => {
        if (favArray.includes(+card.code)) {
          card.isFavourite = true;
        }
        return card;
      });
      this.setState({
        favouriteCardsCodes: favArray,
      });
    } 
    if (localStorage.getItem("cartCounter")) {
      this.setState({
        cartCounter: localStorage.getItem("cartCounter"),
      });
    }
    this.setState({
        cards: cards,
    });
  }

  handleClick=()=>{
    this.setState((prev) => ({ isActiveModal: !prev.isActiveModal }));
  }

  setCardFavourite = (code) => {
    this.setState((prev) => {
      const favArray = [...prev.favouriteCardsCodes]
      let cards = [...prev.cards]
    const index = favArray.indexOf(+code)
    if (index === -1) {
      favArray.push(+code)
    } else {
      favArray.splice(index, 1)
    }
    cards = cards.map((card) => {
      if (card.code === code) {
        card.isFavourite = !card.isFavourite
      }
      return card
    })
    localStorage.setItem("favouriteCardsCodes", JSON.stringify(favArray))
    return { cards: cards, favouriteCardsCodes: favArray }
  })
  }

  setModalProps=(value)=> {
    this.setState({modalProps: value})
  }

  render() {
    const {isActiveModal, cards, modalProps, cartCounter, favouriteCardsCodes} = this.state
    return (
      <div className='App'>
        <Header cartCounter={cartCounter} favouriteCardsCodes={favouriteCardsCodes} />

        <CardContainer setModalProps={this.setModalProps} setCardFavourite={this.setCardFavourite} cards={cards} handleClick={this.handleClick}/>

        {isActiveModal && <Modal onMouseDown={this.handleClick} text={'Do you want to add ' + modalProps.name +' to your shopping cart?'}  action={
          <div className='modal__buttons'>

            <Button text='OK' onClick={() => {
              this.setState((prev) => {
                const cartCounter = +prev.cartCounter + 1
                localStorage.setItem("cartCounter", JSON.stringify(cartCounter))
                return { cartCounter: cartCounter }
              })
              this.handleClick()
            }} />

            <Button text='Cancel' onClick={this.handleClick} />

          </div>
          }
        />}
      </div>
    );
  }
}

export default App;
