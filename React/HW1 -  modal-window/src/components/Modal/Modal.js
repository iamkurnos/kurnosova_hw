import { Component } from "react";
import './Modal.scss'

class Modal extends Component{
    render() {
        const {header, closeButton, text, action, onMouseDown, className} = this.props
        return (
            <div className="modal-container " onMouseDown={event => {
                if (
                    event.target.classList.contains('modal-container') ||
                    event.target.classList.contains('modal__close')
                ) {
                    onMouseDown();
                }
            }}>
                <div className={"modal " + className}>
                    <header>
                        <h2>{header}</h2>
                        {closeButton && <img className="modal__close" src="./img/close.svg" alt="close" />}
                    </header>
                    <div className="modal__main">
                        <p>{text}</p>
                        {action}
                    </div>
                </div>
            </div>
        )
    }
}

export default Modal