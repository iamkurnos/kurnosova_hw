import React, {Component} from 'react';
import './App.scss';
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';

class App extends Component {
  state = {
    isActiveModal: false,
    buttonClick: '',
  }
  handleClick =()=>{
    this.setState((prev) => ({isActiveModal: !prev.isActiveModal}));
  }
  render() {
    const {isActiveModal} = this.state
    return (
      <div className='App'>
        <Button backgroundColor='rgb(99, 99, 203)' text='Open first modal' onClick={() => {
          this.setState({buttonClick: 'first'});
          this.handleClick()
        }} />
        <Button backgroundColor='rgb(47, 129, 94)' text='Open second modal' onClick={() => {
          this.setState({buttonClick: 'second'});
          this.handleClick()
        }} />
        {(isActiveModal && this.state.buttonClick ==='first') && <Modal header='Do you want to delete this file?' closeButton={true} onMouseDown={this.handleClick} text='Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?' className='modal-first'  action={
              <div className='modal__buttons'>
                <Button backgroundColor='rgb(179, 56, 44)' text='OK' onClick={() => {
                  alert('File deleted')
                  this.handleClick()
                }} />
                <Button backgroundColor='rgb(179, 56, 44)' text='Cancel' onClick={this.handleClick } />
              </div>
            }
        />}
        {(isActiveModal && this.state.buttonClick ==='second') && <Modal header='Do you want to send this file?' closeButton={false} onMouseDown={this.handleClick} text='Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit quaerat modi ratione necessitatibus atque ab quam eos magni maiores, expedita voluptate! In ab rem ipsa autem, quibusdam distinctio amet voluptates!' className='modal-second'  action={
              <div className='modal__buttons'>
                <Button backgroundColor='rgb(66, 167, 120)' text='Send' onClick={() => {
                  alert('File sent')
                  this.handleClick()
                }} />
                <Button backgroundColor='rgb(66, 167, 120)' text='Declain' onClick={this.handleClick} />
              </div>
            }
          />}
      </div>
    );
  }
}

export default App;
