const urlIp = 'https://api.ipify.org/?format=json'
const urlGetLocation = 'http://ip-api.com/json/'
const fields = '?fields=continent,country,regionName,city,district'

const btn = document.querySelector('.btn-find-ip')
const locationContainer = document.createElement('div')
btn.after(locationContainer)

const request = async (url) => {
    const response = await fetch(url)
    if (response.ok) {
        const json = await response.json()
        return json
    }
    throw new Error(response.status)
}

btn.addEventListener('click', async() => {
    const dataIp = request(urlIp)
    const userIp = await dataIp.then(({ ip }) => ip)
    
    const location = request(urlGetLocation + userIp + fields)
    
    location.then(locationArray => locationContainer.textContent = `Your location is ${Object.values(locationArray).join(' ')}`)
})