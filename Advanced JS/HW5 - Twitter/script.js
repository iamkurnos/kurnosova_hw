const URL = 'https://ajax.test-danit.com/api/json/'

const root = document.querySelector('#root')

const loader = document.createElement('div')
loader.className = 'loader'
document.body.prepend(loader)

class Requests {
	static get(entity) {
		return fetch(URL + entity, {
			method: "GET",
        }).then(response => {
            const { ok } = response
            if (ok) {
                return response.json()
            }
            throw new Error("Can't complete GET request")
        });
	}

    static post(entity, object) {
        return fetch(URL + entity, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(object),
        }).then(response => {
            const { ok } = response
            if (ok) {
                return response.json()
            }
            throw new Error("Can't complete POST request")

        });
    }

	static put(entity, object, id) {
		return fetch(URL + entity + "/" + id, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify(object),
        }).then(response => {
            const { ok } = response
            if (ok) {
                return response.json()
            } 
            throw new Error("Can't complete PUT request")

        });
	}

	static delete(entity, id) {
		return fetch(URL + entity + "/" + id, {
			method: "DELETE",
		})
	}
}


class Card{
    constructor(id, title, text, name, email,userId) {
        this.id = id
        this.title = title
        this.text = text
        this.name = name
        this.email = email
        this.userId = userId
    }
    create() {
        const cardTemplate = document.querySelector('.cards-elem').content
        const cardWrap = cardTemplate.querySelector('.card').cloneNode(true)

        const cardHeader = cardWrap.querySelector('.card__header')
        cardHeader.textContent = this.title;
        const cardText = cardWrap.querySelector('.card__post')
        cardText.textContent = this.text;

        const userFullName = cardWrap.querySelector('.card__user h4')
        userFullName.textContent = this.name

        const userEmail = cardWrap.querySelector('.card__user span')
        userEmail.textContent = this.email

        const cardDelete = cardWrap.querySelector('.card__delete')
        cardDelete.addEventListener('click', () => {
            Requests.delete('posts', this.id).then(({ ok }) => {
                if (ok) {
                    cardWrap.remove()
                } 
            }).catch((err)=>console.log(err))
        })  

        const cardEdit = cardWrap.querySelector('.card__edit')
        cardEdit.addEventListener('click', () => {
            const form = new Form().createForm(document.body,'Edit your post', this.title, this.text)
            form.querySelector('form').addEventListener('submit', (event) => {
                event.preventDefault()
                form.classList.remove('active-form')

                const newPostObj = {
                    body: form.querySelector('.form__text').value,
                    title: form.querySelector('.form__title').value,
                    userId: this.userId,
                }

                Requests.put('posts', newPostObj, this.id).then(postObj => {
                    cardHeader.textContent = postObj.title
                    cardText.textContent = postObj.body
                }).catch((err)=>console.log(err))
            })
        })
        return cardWrap
    } 
}

class Cards{
    renderAll(container) {
        Requests.get('posts').then(postsArray => {
            Requests.get('users').then(usersArray => {

                postsArray.forEach(({ id, body, title, userId }) => {
                    const user = usersArray.filter(item => item.id === userId)
                    const { name, email } = user[0]
                    const card = new Card(id,title, body, name, email, userId)
                    container.append(card.create())

                })
                loader.remove()
                document.body.classList.remove('preloader')
                root.before(btnAddPost)
            }).catch((err)=>console.log(err))
        }).catch((err)=>console.log(err))
    }

    renderPost(container,postObject) {
        Requests.post('posts', postObject).then(({ id, title, body, name, email, userId }) => {
            const card = new Card(id, title, body, name, email,userId)
            container.prepend(card.create()) 
        }).catch((err)=>console.log(err))
    }
}

const cardsContainer = document.createElement('ul')
cardsContainer.className = 'cards-list'

const cardsList = new Cards()
cardsList.renderAll(cardsContainer)
root.append(cardsContainer)

const btnAddPost = document.createElement('button')
btnAddPost.className = 'add-post-btn'
btnAddPost.textContent = 'ADD POST'

btnAddPost.addEventListener('click', () => {
    const form = new Form().createForm(document.body, 'Create your new post')
    form.querySelector('form').addEventListener('submit', (event) => {
        event.preventDefault()
        form.classList.remove('active-form')

        const newPostObj = {
            body: form.querySelector('.form__text').value,
            title: form.querySelector('.form__title').value,
            name: 'Leanne Graham',
            email: 'Sincere@april.biz',
            userId: 1,
        }
        new Cards().renderPost(cardsContainer,newPostObj)
    })
})

class Form{
    createForm(container, header, title = '', text = '') {
        const formTemplate = document.querySelector('.add-post').content
        const form = formTemplate.querySelector('.form').cloneNode(true)
        const formHeader = form.querySelector('.form__header')
        formHeader.textContent = header
        const formTitle = form.querySelector('.form__title')
        formTitle.value = title
        const formText = form.querySelector('.form__text')
        formText.value = text
        form.classList.add('active-form')
        form.addEventListener('click', (event) => {
            if (event.target.classList.contains('form') || event.target.classList.contains('close-form')) {
                form.classList.remove('active-form')
            }
        })
        container.prepend(form)
        return form
    }
}
