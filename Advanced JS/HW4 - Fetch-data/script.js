const URL = 'https://ajax.test-danit.com/api/swapi/films'


class Films{
    constructor(url) {
        this.url = url
    }
    render(container) {
        fetch(this.url).then(response => response.json())
            .then(films => {
                const list = document.createElement('ul')
                films.forEach(({ name, episodeId, openingCrawl, characters }) => {
                    const episode = document.createElement('li')
                    episode.className = 'episode'
                    const episodeName = document.createElement('h2')
                    episodeName.textContent = name
                    episode.append(episodeName)
                    episode.insertAdjacentHTML('beforeend', `   
                        <div>Episode number: ${episodeId}</div>
                        <p>Plot: ${openingCrawl}</p>`)
                    list.append(episode)
                    const characterWrap = document.createElement('div')
                    characterWrap.className = 'loader'
                    episodeName.after(characterWrap)    
                    new Characters(characters).render(characterWrap)        
                })
                container.prepend(list)    
        }).catch(err =>console.log(err))
        
    }
}

class Characters{
    constructor(charactersArr){
        this.charactersArr = charactersArr.map(url => fetch(url).then(response => response.json()))
    }
    render(container) {
        Promise.allSettled(this.charactersArr).then(characters =>{
            const charactersList = characters.map(person => {
                if (person.status === 'fulfilled') {
                    const { value: { name } } = person
                    return name
                }
            })
            container.textContent = `Characters: ${charactersList.join(', ')}.`
            container.className = 'episode__characters'
        }).catch(err =>console.log(err))
    }
}

const starWarsFilms = new Films(URL)
starWarsFilms.render(document.body)