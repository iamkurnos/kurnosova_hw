class Employee{
    constructor(name, age, salary) {
        this._name = name
        this._age = age
        this._salary = salary
    }
    get name(){
        return this._name
    }
    set name(value){
        return this._name = value
    }
    get age(){
        return this._age
    }
    set age(value){
        return this._age = value
    }
    get salary(){
        return this._salary
    }
    set salary(value){
        return this._salary = value
    }
}

class Programmer extends Employee{
    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this.lang = lang
    }
    get salary(){
        return this._salary*3
    }
    set salary(value){
        return this._salary = value
    }
}

const prog1 = new Programmer('Ben', 24, 2000, 'JS')
const prog2 = new Programmer('Maksim', 32, 4000, 'Java')
const prog3 = new Programmer('Anna', 29, 3000, 'Python')

console.log(prog1);
console.log(prog2);
console.log(prog3);
