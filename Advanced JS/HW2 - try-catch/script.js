const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];

class BookError extends Error{
    constructor(bookProperty) {
        super()
        this.name = 'BookError'
        this.message = `Property ${bookProperty} not found`
    }
}

class CreateBookList{
    constructor(book) {
        this.book = book
    }
    render(container) {
        if (!this.book.name) {
            throw new BookError('name')
        }
        if (!this.book.author) {
            throw new BookError('author')
        }
        if (!this.book.price) {
            throw new BookError('price')
        } 
        container.insertAdjacentHTML('beforeend', `
            <li class="book-list-item">
                <h3>Книга: "${this.book.name}"</h3>
                <div>
                    <span>Автор - ${this.book.author}</span>
                    <span>Ціна: ${this.book.price}грн.</span>
                </div>
            </li>`)      
        
    }
}

const rootContainer = document.querySelector('#root')
const bookList = document.createElement('ul')
rootContainer.append(bookList)

books.forEach(el => {
    try {
        new CreateBookList(el).render(bookList)
    } catch (err) {
            console.log(err)
    }
})