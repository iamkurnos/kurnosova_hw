let firstNum = prompt('Enter the first integer:')
while (isNaN(firstNum) || !Number.isInteger(+firstNum) || /^\s*$/.test(firstNum) || firstNum === null) { firstNum = prompt('Enter the first integer:', firstNum) }

let secondNum = prompt('Enter the second integer:')
while (isNaN(secondNum) || !Number.isInteger(+secondNum) || /^\s*$/.test(secondNum) || secondNum === null) { secondNum = prompt('Enter the second integer:', secondNum) }

let indexNum = prompt('Enter the ordinal number:')
while (isNaN(indexNum) || !Number.isInteger(+indexNum) || /^\s*$/.test(indexNum)) {indexNum = prompt('Enter the ordinal number:', indexNum)}

function factorial(numberOne, numberTwo, index) {
    if (index >= 0) {
        if (index == 0) {
            return numberOne
        } else if (index == 1) {
            return numberTwo
        } else {
            for (i = 2; i <= index; i++) {
                let factorialNum = +numberOne + +numberTwo
                numberOne = numberTwo
                numberTwo = factorialNum
            }
            return numberTwo
        }
    } else {for (i = -1; i >= index; i--) {
                let factorialNum = +numberTwo - +numberOne 
                numberTwo = numberOne
                numberOne = factorialNum
            }
        return numberOne
    }

    
}

document.write(factorial(firstNum, secondNum, indexNum))