function checkDeadline(arrayTeam, arrayTasks, deadlineDate) {
    let daysBeforeDeadline = 0;
    let currentDate = new Date(2021, 11, 14)
   
    for (let i = currentDate; i <= deadlineDate; i.setDate(i.getDate() + 1)) {
        if (i.getDay() !== 6 && i.getDay() !== 0) {
            daysBeforeDeadline += 1
        }
    }

    // while (currentDate <= deadlineDate) {
    //     if (currentDate.getDay() !== 0 && currentDate.getDay() !== 6) {
    //         daysBeforeDeadline += 1
    //     }
    //     currentDate = currentDate.setDate(currentDate.getDate() + 1)
    // }
    
    let productivity = 0;
    for (let point of arrayTeam) {
        productivity += point
    }

    let workload = 0;
    for (let task of arrayTasks) {
        workload += task
    }

    let workDays = workload / productivity
    
    if (workDays <= daysBeforeDeadline) {
        let daysLeft = daysBeforeDeadline - Math.ceil(workDays)
        return `Все задачи будут успешно выполнены за ${daysLeft} дней до наступления дедлайна! ` + daysBeforeDeadline + ' и ' + productivity + ' и ' + workload + ' и ' + workDays
    } else {
        let deficitHours = Math.ceil((workDays - daysBeforeDeadline) * 8)
        return `Команде разработчиков придется потратить дополнительно ${deficitHours} часов после дедлайна, чтобы выполнить все задачи в беклоге ` + daysBeforeDeadline + ' и ' + productivity + ' и ' + workload + ' и ' + workDays
    }
}

let team = [2, 4, 1, 5, 1.5, 2.3, 3.5, 2]
let tasks = [8, 30, 15, 21, 35, 18, 40, 13]
let deadline = new Date(2021, 11, 23)

alert(checkDeadline(team, tasks, deadline))



