let integer = prompt('Enter the non-negative integer:')
while (isNaN(integer) || integer <= 0 || !Number.isInteger(+integer) || /^\s*$/.test(integer)) { integer = prompt('Enter the non-negative integer:', integer) }

function factorial(number) {
    if (number == 1) {
        return number
    } else { return number * factorial(number-1)}
}

document.write(factorial(integer))