$(document).ready(function (){
    $('#nav-page').on('click', 'a', function (event) {
        event.preventDefault();
        let id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1000);
    })
})

$('.js-scroll-top').click(function () {
    $('html, body').animate({
        scrollTop: 0
    }, 1000);
})

$(window).scroll(function () {
    if ($(window).scrollTop() > $(window).height()){
        $('.js-scroll-top').addClass('is-show')
    } else {
        $('.js-scroll-top').removeClass('is-show')
    }
})

$(document).ready(function () {
    $('.posts-toggle').click(function () {
        $('.posts').slideToggle(10, function () {
            if ($(this).is(':hidden')) {
                $('.posts-toggle').html('Show Posts');
            } else {
                $('.posts-toggle').html('Hide Posts')
            }
        })
        return false
    })
})