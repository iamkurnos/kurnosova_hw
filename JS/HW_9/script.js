let tab = document.querySelectorAll('.tabs-title')

let tabContent = document.querySelectorAll('.tabs-content')

tab.forEach((item) => item.addEventListener('click', function (e) {
    e.preventDefault();

    let id = e.target.getAttribute('data-title')

    tab.forEach((elem) => elem.classList.remove('active'))
    tabContent.forEach((elem) => elem.style.display = 'none')
    item.classList.add('active')
    document.getElementById(id).style.display = 'block'
})
 
)

document.querySelector('.tabs-title').click()