let firstNum = prompt('Enter the first number:')
while (isNaN(firstNum) || /^\s*$/.test(firstNum) || firstNum === null) {
    firstNum = prompt('Enter the first number:', firstNum)
}

let secondNum = prompt('Enter the second number:')
while (isNaN(secondNum) || /^\s*$/.test(secondNum) || secondNum === null){
    secondNum = prompt('Enter the second number:', secondNum)
}   

let operation = prompt('What kind of mathematical operation must be effect (enter the corresponding value from parentheses): addition (+), subtraction (-), multiplication (*), division (/)?')
while (operation != '+' && operation != '-' && operation != '*' && operation != '/') {
    operation = prompt('Choose one of the actions: +, -, *, /.', operation)
}

function mathOperation(numberOne, numberTwo, action) {
    switch (action) {
        case '+': return +numberOne + +numberTwo
            break
        case '-': return +numberOne - +numberTwo
            break
        case '*': return +numberOne * +numberTwo
            break
        case '/':
            if (+numberTwo === 0) {
                return 'The second number is zero. The operation cannot be performed'
            } else { return +numberOne / +numberTwo}
            break
    }
}

console.log(mathOperation(firstNum, secondNum, operation))