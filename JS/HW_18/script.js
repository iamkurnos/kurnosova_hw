function copyObject(obj) {
  const newObj = {};
  for(const i in obj) {
    if (obj[i] instanceof Object) {
      newObj[i] = copyObject(obj[i]);
      continue;
    }
    newObj[i] = obj[i];
  }
  return newObj
}
