let form = document.querySelector('.password-form')

form.addEventListener('click', function (event) {
    if (!event.target.classList.contains('fas')) return
    if (event.target.classList.contains('fa-eye')) {
        event.target.classList.remove('fa-eye')
        event.target.classList.add('fa-eye-slash')
        event.target.parentNode.querySelector('input').setAttribute('type','text')
    } else if (event.target.classList.contains('fa-eye-slash')) {
        event.target.classList.remove('fa-eye-slash')
        event.target.classList.add('fa-eye')
        event.target.parentNode.querySelector('input').setAttribute('type','password')
    }
})

let btn = document.querySelector('.btn')

let enterPassword = document.getElementById('enter-password')
let confirmPassword = document.getElementById('confirm-password')

let mistake = document.createElement('div')
btn.before(mistake)

btn.addEventListener('click', function (event) {
    event.preventDefault()
    if (enterPassword.value !== confirmPassword.value || enterPassword.value === '') {
        mistake.style.color = 'red'
        mistake.textContent = 'Нужно ввести одинаковые значения'
    } else {
        mistake.textContent = ''
        alert('You are welcome')
    }
})