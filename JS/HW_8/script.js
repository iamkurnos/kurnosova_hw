let inputField = document.getElementById('price')

inputField.parentNode.style.display = 'block'

inputField.addEventListener('focus', function () {
    this.style.outline = '1px solid green'
}
)

let span = document.createElement('span')
let div = document.createElement('div')
inputField.parentNode.after(div)

inputField.addEventListener('blur', function () {
    this.style.outline = '0.01px solid grey'
    if (this.value < 0) {
            this.style.outline = '1px solid red'
            div.textContent = 'Please enter correct price'
    } else {
    div.textContent = ''       
    span.textContent = `Текущая цена: ${this.value}`
    inputField.before(span)
    inputField.style.color = 'green'
    let btn = document.createElement('button')
    btn.textContent = 'x'
    span.append(btn)
    span.style.display = 'block'   

    btn.addEventListener('click', function () {
        span.remove()
        inputField.value = ''
    })
    }
}
)
