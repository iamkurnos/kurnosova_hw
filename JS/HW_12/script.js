let images = document.querySelectorAll('img')

let btnStop = document.createElement('button')
btnStop.textContent = 'Прекратить'
btnStop.style.cssText = 'margin: 10px; width: 100px; height: 30px; display: block;'
document.body.append(btnStop)

let btnRestore = btnStop.cloneNode()
btnRestore.textContent = 'Возобновить показ'
btnStop.after(btnRestore)

let i = 0
let timer

showImage()

function showImage() {
    images.forEach((item) => item.style.display = 'none')
    images[i].style.display = 'block'
    i++
    if (i == images.length) {
        i = 0
    }   
    timer = setTimeout(showImage, 3000)
}

btnStop.addEventListener('click', () => {
    clearTimeout(timer)
})

btnRestore.addEventListener('click', () => {
    showImage()
})

