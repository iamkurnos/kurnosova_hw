let range = +prompt('Enter the number:')
while (isNaN(range) || range / Math.floor(range) !== 1) {
    range = +prompt('Enter the number:')
}

if (range > 0) {
    if (range < 5) {
    console.log('Sorry, no numbers')
    } else {
        for (let i = 0; i <= range; ++i) {
            if (i % 5 === 0) {
            console.log(i)
            }
        }
    }
} else if (range > -5) {
    console.log('Sorry, no numbers')
    } else {
        for (let i = 0; i >= range; --i){
            if (i % 5 === 0) {
            console.log(i)
            }
        }
    }


