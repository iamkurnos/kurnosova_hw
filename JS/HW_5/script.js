"use strict";

function createNewUser(firstName, lastName) {
    let birthday = prompt('Enter your date of birth in the format dd.mm.yyyy:')

    let newUser = {
        firstName,
        lastName,
        birthday,
        getLogin() {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase()
        },

        setFirstName(newName) {
            Object.defineProperty(this, 'firstName', {value: newName})
        },

        setLastName(newLastName) {
            Object.defineProperty(this, 'lastName', {value: newLastName})          
        },

        getAge() {
            let ageDate = new Date(+this.birthday.slice(-4), +this.birthday.slice(4, 6) - 1, +this.birthday.slice(0, 2))
            let userAge = Math.floor((new Date() - +ageDate) / (1000 * 60 * 60 * 24 * 365))
            return userAge
        },

        getPassword() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4) 
        },
    }
    Object.defineProperty(newUser, 'firstName', {writable: false, configurable: true});
    Object.defineProperty(newUser, 'lastName', { writable: false, configurable: true });
    return newUser
}

function check(dataQuestion) {
    let info = prompt(dataQuestion)
    while (/^\s*$/.test(info) || info === null) {
        info = prompt(dataQuestion)
    }
    return info
}

let firstUser = createNewUser(check('Enter your name:'), check('Enter your surname:'))

firstUser.getLogin()

console.log(firstUser.getLogin())

console.log(firstUser)

firstUser.getAge()

console.log(firstUser.getAge())

firstUser.getPassword()

console.log(firstUser.getPassword())

