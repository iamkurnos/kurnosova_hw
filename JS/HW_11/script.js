let btn = document.querySelectorAll('.btn')


window.addEventListener('keyup', function (e) {
    let content = e.key.toLowerCase()
    
    btn.forEach(function (item) {
        item.classList.remove('active')

        if (item.getAttribute('data-toggle') === content) {
            item.classList.add('active')
        }
    })
})

    