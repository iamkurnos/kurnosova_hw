"use strict";

function createNewUser(firstName, lastName) {
    let newUser = {
        firstName,
        lastName,
        getLogin() {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase()
        },

        setFirstName(newName) {
            Object.defineProperty(this, 'firstName', {value: newName})
        },

        setLastName(newLastName) {
            Object.defineProperty(this, 'lastName', {value: newLastName})          
        }
    }
    Object.defineProperty(newUser, 'firstName', {writable: false, configurable: true});
    Object.defineProperty(newUser, 'lastName', { writable: false, configurable: true });
    return newUser
}

function check(dataQuestion) {
    let info = prompt(dataQuestion)
    while (/^\s*$/.test(info) || info === null) {
        info = prompt(dataQuestion)
    }
    return info
}

let firstUser = createNewUser(check('Enter your name:'), check('Enter your surname:'))

firstUser.getLogin()

console.log(firstUser.getLogin())

