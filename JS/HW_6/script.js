function filterBy(array, dataType) {
    let newArray = []
    for (let i = 0; i < array.length; i++) {
        if (dataType === 'null') {
            if (array[i] !== null) {
                newArray.push(array[i])
            }
        } else if (dataType === 'object') {
            if (typeof array[i] !== dataType && typeof array[i] !== 'function' || array[i] === null) {
                newArray.push(array[i])
            }
        } else {
            if (typeof array[i] !== dataType) {
                newArray.push(array[i])
            }
        }
    }
    return newArray
}