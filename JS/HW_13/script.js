let btn = document.querySelector('.btn')

function changeTheme() {
    document.body.classList.toggle('bgchanger')
    document.querySelectorAll('.tour h4').forEach((item) => item.classList.toggle('changeColor'))
    document.querySelectorAll('.contacts a').forEach((item) => item.classList.toggle('changeColor'))
    document.querySelector('.logo').classList.toggle('changeBGC')
    btn.classList.toggle('changeBGC')
    document.querySelector('.copyright').classList.toggle('changeColor')
        if (document.body.classList.contains('bgchanger')) {
        localStorage.setItem('theme', 'light')
    } else {localStorage.removeItem('theme')} 
}

window.onload = function () {
    if (localStorage.getItem('theme') !== null) {
        changeTheme()
    }
}

btn.addEventListener('click', changeTheme)
