function arrayList(array, parent = document.body) {
    let ul = document.createElement('ul');
    const list = array.map(function (item) {
        if (Array.isArray(item)) {
            return arrayList(item, ul)
        } else {    
            return `<li>${item}</li>`
        }
        }).join('')

    ul.innerHTML = list;
    
    parent.append(ul)
    
    return `<ul>${ul.innerHTML}</ul>`
}


let firstArray = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

let h1 = document.querySelector('h1')

arrayList(firstArray, h1)

let div = document.createElement('div')
h1.prepend(div)
let sec = 3
div.textContent = sec
let timer = setInterval(() => {
    sec--
    div.textContent = sec
}, 1000)

setTimeout(() => {
    clearInterval(timer)
    document.body.textContent = ''
}, 3000)

