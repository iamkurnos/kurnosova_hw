//     1. var - объявление переменной с глобальной областью видимости; у let и const область видимости ограничена блоком, в котором они объявлены. Let могут присваиваться новые значения, а const является постоянной и не изменяется.
//     2. Т.к. область видимости var глобальная, то могут возникать сложности, например, если будет создана переменная с таким же именем и другим значением внутри блока, то переменная var перезапишется. Var является более устаревшим способом объявления переменной.


let userName
let userAge

do {
    userName = prompt('Enter your name:')
} while (
    /^\s*$/.test(userName)
)

if (!!userName === true) {

    do {
    userAge = prompt('How old are you?')
    } while (isNaN(userAge))

    if (userAge < 18) {
        alert('You are not allowed to visit this website')
    } else if (userAge >= 18 && userAge <= 22) {
    if (confirm('Are you sure you want to continue?')) {
        alert(`Welcome, ${userName}`)
    } else {
        alert('You are not allowed to visit this website')
    }
    } else {
        alert(`Welcome, ${userName}`)
    }
}
    
    
    

